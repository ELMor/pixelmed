#!/bin/sh

java -Djavax.xml.transform.TransformerFactory=org.apache.xalan.xsltc.trax.TransformerFactoryImpl -Xmx512m -Xms512m  -cp "./pixelmed.jar:./hsqldb.jar:./xmlpack.jar:./excalibur-bzip2-1.0.jar:./vecmath1.2-1.14.jar" com.pixelmed.display.DicomImageViewer
