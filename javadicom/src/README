20031105. DAC.

This is the PixelMed Publishing pure Java DICOM toolkit. All the
class and supporting files are included in a single jar file,
pixelmed.jar.

Installation.

The following additional jar files from other freely available packages
are required:

hsqldb.jar

		The HypersonicSQL database. The recommended version is
		1.7.2. See "http://hsqldb.sourceforge.net/".

xmlpack.jar

		Several components extracted from the Sun Java Web Services
		Developer Pack, which packages much of the necessary Apache
		stuff for xml and xslt support. The recommended version is
		1.1. During the "installation" of what you download from
		Sun you only need the JAXP (Java API for XML Processing)
		stuff (it is currently version 1.2.2). From the downloaded
		JAXP files, copy the following jar files into the current
		directory:
		
		jwsdp-1.1/jaxp-1.2.2/lib/jaxp-api.jar
		jwsdp-1.1/jaxp-1.2.2/lib/endorsed/dom.jar
		jwsdp-1.1/jaxp-1.2.2/lib/endorsed/sax.jar
		jwsdp-1.1/jaxp-1.2.2/lib/endorsed/xalan.jar
		jwsdp-1.1/jaxp-1.2.2/lib/endorsed/xercesImpl.jar
		jwsdp-1.1/jaxp-1.2.2/lib/endorsed/xsltc.jar
		
		which our Makefile can then combine into a single xmlpack.jar
		file by running "make xmlpack.jar".

		See "http://java.sun.com/webservices/downloads/webservicespack.html".

excalibur-bzip2-1.0.jar

		A pure Java implementation of the bzip2 compression algorithm. Even
		though this is only used to support a private PixelMed compression
		transfer syntax, the jar file is required at runtime regardless. It
		is available from the Apache project:
		
		"http://www.apache.org/dist/avalon/excalibur/components/deprecated/bzip2/"

		It was formerly part of the Apache Jakarta Avalon Excalibur project
		as a component, but has now been folded into the Ant distribution,
		which is why it is flagged as deprecated.
		
vecmath1.2-1.14.jar

		The computation of geometric information (such as for posting localizers
		and reporting 3D cursor positions) for cross-sectional images depends
		on the Java 3D API, and since this isn't available for all platforms,
		the code makes use of the routines in this free 3rd party implementation
		of the vector math (only) parts of the 3D API written by Kenji Hiranabe:

		"http://objectclub.esm.co.jp/vecmath/"
		
		Note that the official Sun 3D version has not yet been tested with
		the pixelmed code.

Invocation.

One the various jar files have been assembled, applications in the DICOM toolkit can be
invoked as follows, using the com.pixelmed.display.DicomImageViewer as an example:

java -Xmx512m -Xms512m  -cp "./pixelmed.jar:./hsqldb.jar:./xmlpack.jar:./excalibur-bzip2-1.0.jar:./vecmath1.2-1.14.jar" com.pixelmed.display.DicomImageViewer

Note that for practical display or processing of large DICOM images a lot of heap
space is necessary, hence the "-Xmx512m -Xms512m".

Development Documentation.

To use the toolkit for your own applications, read the documentation of the classes
provided, both as traditional Javadoc (see docs/javadoc/index.html) and as Doxygen
(see docs/doxygen/html/index.html).






 
