/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import com.pixelmed.utils.HexDump;

// JAXP packages
import javax.xml.parsers.*;
//import org.xml.sax.*;
//import org.xml.sax.helpers.*;
import org.w3c.dom.*;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.sun.org.apache.xml.internal.serialize.LineSeparator;

import java.io.*;
import java.util.*;

/**
 * <p>A class to encode a representation of a DICOM object in an XML form,
 * suitable for analysis as human-readable text, or for feeding into an
 * XSLT-based validator.
 *
 * <p>A typical example of usage would be:</p>
 * <pre>
try {
    AttributeList list = new AttributeList();
    list.read("dicomfile",null,true,true);
    Document document = new XMLRepresentationOfDicomObjectFactory().getDocument(list);
    XMLRepresentationOfDicomObjectFactory.write(System.out,document);
} catch (Exception e) {
    e.printStackTrace(System.err);
 }
 * </pre>
 *
 * <p>or even simpler, if there is no further use for the XML document:</p>
 * <pre>
try {
    AttributeList list = new AttributeList();
    list.read("dicomfile",null,true,true);
    XMLRepresentationOfDicomObjectFactory.createDocumentAndWriteIt(list,System.out);
} catch (Exception e) {
    e.printStackTrace(System.err);
 }
 * </pre>
 *
 * @see com.pixelmed.validate
 * @see org.w3c.dom.Document
 *
 * @author	dclunie
 */
public class XMLRepresentationOfDicomObjectFactory {

	private static final String identString = "@(#) $Header$";

	/***/
	private DocumentBuilder db;
	
	/**
	 * @param	tag
	 */
	private String makeElementNameFromHexadecimalGroupElementValues(AttributeTag tag) {
		StringBuffer str = new StringBuffer();
		str.append("HEX");		// XML element names not allowed to start with a number
		String groupString = Integer.toHexString(tag.getGroup());
		for (int i=groupString.length(); i<4; ++i) str.append("0");
		str.append(groupString);
		String elementString = Integer.toHexString(tag.getElement());
		for (int i=elementString.length(); i<4; ++i) str.append("0");
		str.append(elementString);
		return str.toString();
	}

	/**
	 * @param	list
	 * @param	document
	 * @param	parent
	 */
	private void addAttributesFromListToNode(AttributeList list,Document document,Node parent) {
		DicomDictionary dictionary = list.getDictionary();
		Iterator i = list.values().iterator();
		while (i.hasNext()) {
			Attribute attribute = (Attribute)i.next();
			AttributeTag tag = attribute.getTag();
			
			String elementName = dictionary.getNameFromTag(tag);
			if (elementName == null) {
				elementName=makeElementNameFromHexadecimalGroupElementValues(tag);
			}
			Node node = document.createElement(elementName);
			parent.appendChild(node);
			
			{
				Attr attr = document.createAttribute("group");
				attr.setValue(HexDump.shortToPaddedHexString(tag.getGroup()));
				node.getAttributes().setNamedItem(attr);
			}
			{
				Attr attr = document.createAttribute("element");
				attr.setValue(HexDump.shortToPaddedHexString(tag.getElement()));
				node.getAttributes().setNamedItem(attr);
			}
			{
				Attr attr = document.createAttribute("vr");
				attr.setValue(ValueRepresentation.getAsString(attribute.getVR()));
				node.getAttributes().setNamedItem(attr);
			}
			
			if (attribute instanceof SequenceAttribute) {
				int count=0;
				Iterator si = ((SequenceAttribute)attribute).iterator();
				while (si.hasNext()) {
					SequenceItem item = (SequenceItem)si.next();
					Node itemNode = document.createElement("Item");
					Attr numberAttr = document.createAttribute("number");
					numberAttr.setValue(Integer.toString(++count));
					itemNode.getAttributes().setNamedItem(numberAttr);
					node.appendChild(itemNode);
					addAttributesFromListToNode(item.getAttributeList(),document,itemNode);
				}
			}
			else {
				//Attr attr = document.createAttribute("value");
				//attr.setValue(attribute.getDelimitedStringValuesOrEmptyString());
				//node.getAttributes().setNamedItem(attr);
				
				//node.appendChild(document.createTextNode(attribute.getDelimitedStringValuesOrEmptyString()));
				
				String values[] = null;
				try {
					values=attribute.getStringValues();
				}
				catch (DicomException e) {
					//e.printStackTrace(System.err);
				}
				if (values != null) {
					for (int j=0; j<values.length; ++j) {
						Node valueNode = document.createElement("value");
						Attr numberAttr = document.createAttribute("number");
						numberAttr.setValue(Integer.toString(j+1));
						valueNode.getAttributes().setNamedItem(numberAttr);
						valueNode.appendChild(document.createTextNode(values[j]));
						node.appendChild(valueNode);
					}
				}
			}
		}
	}

	/**
	 * @param	list
	 * @param	document
	 */
	private void addAttributesFromListToDocument(AttributeList list,Document document) {
		Node root = document.createElement("DicomObject");
		document.appendChild(root);
		addAttributesFromListToNode(list,document,root);
	}

	/**
	 * <p>Construct a factory object, which can be used to get XML documents from DICOM objects.</p>
	 *
	 * @exception	ParserConfigurationException
	 */
	public XMLRepresentationOfDicomObjectFactory() throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		db = dbf.newDocumentBuilder();
	}
	
	/**
	 * <p>Given a DICOM object encoded as a list of attributes, get an XML document
	 * as a DOM tree.</p>
	 *
	 * @param	list	the list of DICOM attributes
	 */
	public Document getDocument(AttributeList list) {
		Document document = db.newDocument();
		addAttributesFromListToDocument(list,document);
		return document;
	}
	
	/**
	 * @param	node
	 * @param	indent
	 */
	public static String toString(Node node,int indent) {
		StringBuffer str = new StringBuffer();
		for (int i=0; i<indent; ++i) str.append("    ");
		str.append(node);
		if (node.hasAttributes()) {
			NamedNodeMap attrs = node.getAttributes();
			for (int j=0; j<attrs.getLength(); ++j) {
				Node attr = attrs.item(j);
				//str.append(toString(attr,indent+2));
				str.append(" ");
				str.append(attr);
			}
		}
		str.append("\n");
		++indent;
		for (Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
			str.append(toString(child,indent));
			//str.append("\n");
		}
		return str.toString();
	}
	
	/**
	 * @param	node
	 */
	public static String toString(Node node) {
		return toString(node,0);
	}
	
	/**
	 * <p>Serialize an XML document (DOM tree).</p>
	 *
	 * @param	out		the output stream to write to
	 * @param	document	the XML document
	 * @exception	IOException
	 */
	public static void write(OutputStream out,Document document) throws IOException {
		OutputFormat format = new OutputFormat();	// defaults to xml and UTF-8
		//format.setLineSeparator(LineSeparator.Windows);
		format.setIndenting(true);
		format.setLineWidth(0);             
		//format.setPreserveSpace(true);
		XMLSerializer serializer = new XMLSerializer (new OutputStreamWriter(out),format);
		serializer.asDOMSerializer();
		serializer.serialize(document);
	}
	
	/**
	 * <p>Serialize an XML document (DOM tree) created from a DICOM attribute list.</p>
	 *
	 * @param	list		the list of DICOM attributes
	 * @param	out		the output stream to write to
	 * @exception	IOException
	 * @exception	DicomException
	 */
	public static void createDocumentAndWriteIt(AttributeList list,OutputStream out) throws IOException, DicomException {
		try {
			Document document = new XMLRepresentationOfDicomObjectFactory().getDocument(list);
			write(out,document);
		}
		catch (ParserConfigurationException e) {
			throw new DicomException("Could not create XML document "+e);
		}
	}
		
	/**
	 * <p>Read a DICOM dataset and write an XML representation of it to the standard output.</p>
	 *
	 * @param	arg	one filename of the file containing the DICOM dataset
	 */
	public static void main(String arg[]) {
		try {
			AttributeList list = new AttributeList();
			//System.err.println("reading list");
			list.read(arg[0],null,true,true);
			//System.err.println("making document");
			Document document = new XMLRepresentationOfDicomObjectFactory().getDocument(list);
			//System.err.println(toString(document));
			write(System.out,document);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}
}

