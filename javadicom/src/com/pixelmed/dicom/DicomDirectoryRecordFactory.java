/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

/**
 * @author	dclunie
 */
public class DicomDirectoryRecordFactory {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	class TopDirectoryRecord extends DicomDirectoryRecord {
		/***/
		TopDirectoryRecord() {
			super(null,null);
			uid="";
		}

		/***/
		public String toString() {
			return "Top";
		}

		/***/
		protected void makeStringValue() {
			stringValue="BAD";
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=-1;
		}
	}

	/***/
	class UnrecognizedDirectoryRecord extends DicomDirectoryRecord {
		/***/
		private String directoryRecordType;

		/***/
		UnrecognizedDirectoryRecord() {
			super(null,null);
			directoryRecordType="Unrecognized";
		}

		/**
		 * @param	parent
		 */
		UnrecognizedDirectoryRecord(DicomDirectoryRecord parent) {
			super(parent,null);
			directoryRecordType="Unrecognized";
		}

		/**
		 * @param	parent
		 * @param	list
		 */
		UnrecognizedDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			directoryRecordType="Unrecognized";
		}

		/**
		 * @param	parent
		 * @param	list
		 * @param	name
		 */
		UnrecognizedDirectoryRecord(DicomDirectoryRecord parent,AttributeList list,String name) {
			super(parent,list);
			uid="";
			directoryRecordType=name;
		}

		/***/
		public String toString() {
			return directoryRecordType;
		}

		/***/
		protected void makeStringValue() {
			stringValue="BAD";
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=-1;
		}
	}

	/***/
	class PatientDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		PatientDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid="";
		}

		/***/
		public String toString() {
			return "Patient "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			// do NOT use compareToByStringValue() else absence of UIDs will cause erroneous equality
			return toString().compareTo(((DicomDirectoryRecord)o).toString());		// includes name of record type
		}

		/***/
		protected void makeStringValue() {
			StringBuffer buffer = new StringBuffer();
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.PatientName));
			buffer.append(" ");
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.PatientID));
			stringValue=buffer.toString();
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=-1;
		}
	}

	/***/
	class StudyDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		StudyDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.StudyInstanceUID);
		}

		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByStringValue((DicomDirectoryRecord)o);
		}

		/***/
		public String toString() {
			return "Study "+getStringValue();
		}
	
		/***/
		protected void makeStringValue() {
			StringBuffer buffer = new StringBuffer();
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.StudyDate));	// [bugs.mrmf] (000111) Studies in browser not sorted by date but ID, and don't display date
			buffer.append(" ");
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.StudyID));
			buffer.append(" ");
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.StudyDescription));
			stringValue=buffer.toString();
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=-1;
		}
	}

	/***/
	class SeriesDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		SeriesDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.SeriesInstanceUID);
		}

		/***/
		public String toString() {
			return "Series "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByIntegerValue((DicomDirectoryRecord)o);
		}

		/***/
		protected void makeStringValue() {
			stringValue=DescriptionFactory.makeSeriesDescription(getAttributeList());
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.SeriesNumber,-1);
		}
	}

	/***/
	class ConcatenationDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		ConcatenationDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.ConcatenationUID);
//System.err.println("ConcatenationDirectoryRecord:");
//System.err.println(list);
		}

		/***/
		public String toString() {
			return "Concatenation "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByIntegerValue((DicomDirectoryRecord)o);
		}

		/***/
		protected void makeStringValue() {
			StringBuffer buffer = new StringBuffer();
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.InstanceNumber));
			stringValue=buffer.toString();
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InstanceNumber,-1);
		}
	}

	/***/
	class ImageDirectoryRecord extends DicomDirectoryRecord {
		/***/
		String recordNameFromSOPClass;
		
		/**
		 * @param	parent
		 * @param	list
		 */
		ImageDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.ReferencedSOPInstanceUIDInFile);
			String sopClassUID = Attribute.getSingleStringValueOrNull(list,TagFromName.ReferencedSOPClassUIDInFile);
			recordNameFromSOPClass="Image";
			if (sopClassUID != null) {
				if      (SOPClass.isSpectroscopy(sopClassUID)) recordNameFromSOPClass="Spectra";
				else if (SOPClass.isRawData(sopClassUID)) recordNameFromSOPClass="Raw Data";
			}
		}

		/***/
		public String toString() {
			return recordNameFromSOPClass+" "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByIntegerValue((DicomDirectoryRecord)o);
		}

		/***/
		protected void makeStringValue() {
			stringValue=DescriptionFactory.makeImageDescription(getAttributeList());
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InConcatenationNumber,-1);
			if (integerValue == -1) {
				integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InstanceNumber,-1);
			}
		}
	}

	/***/
	class SpectroscopyDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		SpectroscopyDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.ReferencedSOPInstanceUIDInFile);
		}

		/***/
		public String toString() {
			return "Spectra "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByIntegerValue((DicomDirectoryRecord)o);
		}

		/***/
		protected void makeStringValue() {
			StringBuffer buffer = new StringBuffer();
			String useNumber=Attribute.getSingleStringValueOrNull(getAttributeList(),TagFromName.InConcatenationNumber);
			if (useNumber == null) useNumber=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.InstanceNumber);
			buffer.append(useNumber);
			buffer.append(" ");
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.ImageComments));
			stringValue=buffer.toString();
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InConcatenationNumber,-1);
			if (integerValue == -1) {
				integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InstanceNumber,-1);
			}
		}
	}

	/***/
	class RawDataDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		RawDataDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.ReferencedSOPInstanceUIDInFile);
		}

		/***/
		public String toString() {
			return "Raw Data "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByIntegerValue((DicomDirectoryRecord)o);
		}

		/***/
		protected void makeStringValue() {
			StringBuffer buffer = new StringBuffer();
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.InstanceNumber));
			stringValue=buffer.toString();
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InstanceNumber,-1);
		}
	}

	/***/
	class SRDocumentDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		SRDocumentDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.ReferencedSOPInstanceUIDInFile);
		}

		/***/
		public String toString() {
			return "SR Document "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByIntegerValue((DicomDirectoryRecord)o);
		}

		/***/
		protected void makeStringValue() {
			StringBuffer buffer = new StringBuffer();
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.InstanceNumber));
			stringValue=buffer.toString();
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InstanceNumber,-1);
		}
	}

	/***/
	class WaveformDirectoryRecord extends DicomDirectoryRecord {
		/**
		 * @param	parent
		 * @param	list
		 */
		WaveformDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
			super(parent,list);
			uid=Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.ReferencedSOPInstanceUIDInFile);
		}

		/***/
		public String toString() {
			return "Waveform "+getStringValue();
		}
	
		/**
		 * @param	o
		 */
		public int compareTo(Object o) {
			return compareToByIntegerValue((DicomDirectoryRecord)o);
		}

		/***/
		protected void makeStringValue() {
			StringBuffer buffer = new StringBuffer();
			buffer.append(Attribute.getSingleStringValueOrEmptyString(getAttributeList(),TagFromName.InstanceNumber));
			stringValue=buffer.toString();
		}

		/***/
		protected void makeIntegerValue() {
			integerValue=Attribute.getSingleIntegerValueOrDefault(getAttributeList(),TagFromName.InstanceNumber,-1);
		}
	}

	/**
	 * @param	parent
	 * @param	list
	 */
	public DicomDirectoryRecord getNewDicomDirectoryRecord(DicomDirectoryRecord parent,AttributeList list) {
		DicomDirectoryRecord record = null;

		try {
			if (list == null) {
				record = new UnrecognizedDirectoryRecord(parent);
			}
			else {
//System.err.println("getNewDicomDirectoryRecord: "+list);
				String directoryRecordType=list.get(TagFromName.DirectoryRecordType).getStringValues()[0];
				if (directoryRecordType == null) {
					record = new UnrecognizedDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("PATIENT")) {
					record = new PatientDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("STUDY")) {
					record = new StudyDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("SERIES")) {
					record = new SeriesDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("CONCATENATION")) {
					record = new ConcatenationDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("IMAGE")) {
					record = new ImageDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("SR DOCUMENT")) {
					record = new SRDocumentDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("WAVEFORM")) {
					record = new WaveformDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("SPECTROSCOPY")) {
					record = new SpectroscopyDirectoryRecord(parent,list);
				}
				else if (directoryRecordType.equals("RAW DATA")) {
					record = new RawDataDirectoryRecord(parent,list);
				}
				else {
					record = new UnrecognizedDirectoryRecord(parent,list,directoryRecordType);
				}
			}
		}
		catch (DicomException e) {
			record = new UnrecognizedDirectoryRecord(parent,list);
		}

		return record;
	}

	/***/
	public TopDirectoryRecord getNewTopDirectoryRecord() {
		return new TopDirectoryRecord();
	}

}


