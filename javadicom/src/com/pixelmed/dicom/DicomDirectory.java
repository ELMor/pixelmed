/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.*;
import java.io.File;

/**
 * @author	dclunie
 */
public class DicomDirectory implements TreeModel {

	/***/
	private static final String identString = "@(#) $Header$";

	// Our nodes are all instances of DicomDirectoryRecord ...

	/***/
	private DicomDirectoryRecord root;

	// Stuff to support listener vector

	/***/
	private Vector listeners;

	// Methods for TreeModel

	/**
	 * @param	node
	 * @param	index
	 */
	public Object getChild(Object node,int index) {
		return ((DicomDirectoryRecord)node).getChildAt(index);
	}

	/**
	 * @param	parent
	 * @param	child
	 */
	public int getIndexOfChild(Object parent, Object child) {
		return ((DicomDirectoryRecord)parent).getIndex((DicomDirectoryRecord)child);
	}

	/***/
	public Object getRoot() { return root; }

	/**
	 * @param	parent
	 */
	public int getChildCount(Object parent) {
		return ((DicomDirectoryRecord)parent).getChildCount();
	}

	/**
	 * @param	node
	 */
	public boolean isLeaf(Object node) {
		return ((DicomDirectoryRecord)node).getChildCount() == 0;
	}

	/**
	 * @param	path
	 * @param	newValue
	 */
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	/**
	 * @param	tml
	 */
	public void addTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners = new Vector();
		listeners.addElement(tml);
	}

	/**
	 * @param	tml
	 */
	public void removeTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners.removeElement(tml);
	}

	// Methods specific to DicomDirectory

	/***/
	private TreeMap mapOffsetToSequenceItemAttributeList;
	/***/
	private DicomDirectoryRecordFactory nodeFactory;
	
	/**
	 * @param	node
	 * @param	wantConcatenationUID
	 * @param	useInstanceNumber
	 * @exception	DicomException
	 */
	private DicomDirectoryRecord findOrInsertNewConcatenationDirectoryRecord(DicomDirectoryRecord node,String wantConcatenationUID,String useInstanceNumber) throws DicomException {
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: "+wantConcatenationUID);
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: searching parent "+node);
		DicomDirectoryRecord found = null;
		int n = getChildCount(node);
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: child count="+n);
		for (int i=0; i<n; ++i) {
			DicomDirectoryRecord child=(DicomDirectoryRecord)getChild(node,i);
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: examining child "+child);
			if (child instanceof DicomDirectoryRecordFactory.ConcatenationDirectoryRecord) {
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: have ConcatenationDirectoryRecord");
				AttributeList list = child.getAttributeList();
				String haveConcatenationUID = Attribute.getSingleStringValueOrNull(list,TagFromName.ConcatenationUID);
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: comparing with existing ConcatenationDirectoryRecord "+haveConcatenationUID);
				if (haveConcatenationUID != null && wantConcatenationUID != null && haveConcatenationUID.equals(wantConcatenationUID)) {
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: match");
					found=child;
					break;
				}
			}
		}
		if (found == null) {
//System.err.println("findOrInsertNewConcatenationDirectoryRecord: making new one");
			AttributeList list = new AttributeList();
			Attribute directoryRecordType = new CodeStringAttribute(TagFromName.DirectoryRecordType);
			directoryRecordType.addValue("CONCATENATION");
			list.put(TagFromName.DirectoryRecordType,directoryRecordType);
			Attribute concatenationUID = new UniqueIdentifierAttribute(TagFromName.ConcatenationUID);
			concatenationUID.addValue(wantConcatenationUID);
			list.put(TagFromName.ConcatenationUID,concatenationUID);
			if (useInstanceNumber != null) {
				Attribute instanceNumber = new CodeStringAttribute(TagFromName.InstanceNumber);
				instanceNumber.addValue(useInstanceNumber);
				list.put(TagFromName.InstanceNumber,instanceNumber);
			}
			found=nodeFactory.getNewDicomDirectoryRecord(node,list);
			node.addChild(found);
		}
		return found;
	}
	
	/**
	 * @param	node
	 * @exception	DicomException
	 */
	private void insertConcatenationNodes(DicomDirectoryRecord node) throws DicomException {
//System.err.println("insertConcatenationNodes:");
		int n = getChildCount(node);
		int i=0;
		while (i<n) {
			DicomDirectoryRecord child=(DicomDirectoryRecord)getChild(node,i);
			if (node instanceof DicomDirectoryRecordFactory.SeriesDirectoryRecord && child instanceof DicomDirectoryRecordFactory.ImageDirectoryRecord) {
//System.err.println("insertConcatenationNodes: testing child ["+i+"]");
				AttributeList list = child.getAttributeList();
				String concatenationUID = Attribute.getSingleStringValueOrNull(list,TagFromName.ConcatenationUID);
				if (concatenationUID != null) {
					String instanceNumber = Attribute.getSingleStringValueOrNull(list,TagFromName.InstanceNumber);
					DicomDirectoryRecord concatenation = findOrInsertNewConcatenationDirectoryRecord(node,concatenationUID,instanceNumber);
//System.err.println("insertConcatenationNodes:concatenation in series is: "+concatenation);
//System.err.println("insertConcatenationNodes:removing child from series: "+child);
					node.removeChild(child);
//System.err.println("insertConcatenationNodes:adding child to concatenation:");
					concatenation.addChild(child);
					// restart the scan since the list has changed ... (and take care not to inadvertantly immediately increment i !)
					i=0;
					n=getChildCount(node);
				}
				else {
					++i;
				}
			}
			else {
				insertConcatenationNodes(child);
				++i;
			}
		}
	}

	/**
	 * @param	parent
	 * @param	offset
	 * @exception	DicomException
	 */
	private DicomDirectoryRecord processSubTree(DicomDirectoryRecord parent,long offset) throws DicomException {
//System.err.println("processSubTree:");

		AttributeList list = (AttributeList)(mapOffsetToSequenceItemAttributeList.get(new Long(offset)));
		DicomDirectoryRecord node = nodeFactory.getNewDicomDirectoryRecord(parent,list);

		long offsetOfFirstChild = list.get(TagFromName.LowerLevelDirectoryOffset).getLongValues()[0];
		if (offsetOfFirstChild != 0) {
//System.err.println("processSubTree: addChild offset=0x"+Long.toHexString(offsetOfFirstChild)+" to node "+node);
			node.addChild(processSubTree(node,offsetOfFirstChild));
		}

		long offsetOfNextSibling = list.get(TagFromName.NextDirectoryRecordOffset).getLongValues()[0];
		if (offsetOfNextSibling != 0) {
//System.err.println("processSubTree: addSibling offset=0x"+Long.toHexString(offsetOfNextSibling)+" to parent "+parent);
			node.addSibling(processSubTree(parent,offsetOfNextSibling));
		}

		return node;
	}

	/**
	 * @param	list
	 * @param	doConcatenations
	 * @exception	DicomException
	 */
	private void doCommonConstructorStuff(AttributeList list,boolean doConcatenations) throws DicomException {
//long startTime = System.currentTimeMillis();
//System.err.println(list.toString());

		// Step 1 ... traverse entire (linear) directory record sequence
		// and build index of offsets of each sequence item

//System.err.println("Make offset mapping");

		mapOffsetToSequenceItemAttributeList = new TreeMap();

		Iterator i = ((SequenceAttribute)list.get(TagFromName.DirectoryRecordSequence)).iterator();
		while (i.hasNext()) {
			SequenceItem item = (SequenceItem)i.next();
			mapOffsetToSequenceItemAttributeList.put(new Long(item.getByteOffset()),item.getAttributeList());
		}
//long currentTime = System.currentTimeMillis();
//System.err.println("Make offset mapping = "+(currentTime-startTime)+" ms");
//startTime=currentTime;
		// Step 2 ... walk tree starting from root, building our tree

//System.err.println("Walk tree");

		nodeFactory=new DicomDirectoryRecordFactory();

		long offsetOfRoot = list.get(TagFromName.RootDirectoryFirstRecord).getLongValues()[0];
		if (offsetOfRoot != 0) {
			root = nodeFactory.getNewTopDirectoryRecord();		// we create our own (empty) root on top
			root.addChild(processSubTree(root,offsetOfRoot));	// the DICOMDIR "root" is really the first of many siblings
		}
//currentTime = System.currentTimeMillis();
//System.err.println("Walk tree took = "+(currentTime-startTime)+" ms");
//startTime=currentTime;
//System.err.println(toString());

		// Step 3 ... walk tree to insert pseudo-records for concatenations ...
		
		if (doConcatenations) insertConcatenationNodes(root);
//currentTime = System.currentTimeMillis();
//System.err.println("Inserting concatenations took = "+(currentTime-startTime)+" ms");
//startTime=currentTime;
		// Step 4 ... clean up intermediate data structures ...

		mapOffsetToSequenceItemAttributeList=null;
		nodeFactory=null;
	}

	/**
	 * @param	list
	 * @exception	DicomException
	 */
	public DicomDirectory(AttributeList list) throws DicomException {
		doCommonConstructorStuff(list,true);
	}
	
	/**
	 * @param	list
	 * @param	doConcatenations
	 * @exception	DicomException
	 */
	public DicomDirectory(AttributeList list,boolean doConcatenations) throws DicomException {
		doCommonConstructorStuff(list,doConcatenations);
	}
	
	/**
	 * @param	node
	 */
	private String walkTree(DicomDirectoryRecord node) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(node.toString());
		buffer.append("\n");

		int n = getChildCount(node);
		for (int i=0; i<n; ++i) buffer.append(walkTree((DicomDirectoryRecord)getChild(node,i)));

		return buffer.toString();
	}

	/***/
	public String toString() {
		return walkTree(root);
	}

	/***/
	private Map mapOfSOPInstanceUIDToReferencedFileName;

	/**
	 * @param	record
	 * @param	parentFilePath
	 */
	private void addToMapOfSOPInstanceUIDToReferencedFileName(DicomDirectoryRecord record,String parentFilePath) {
		String fileName = null;
		AttributeList list = record.getAttributeList();
		if (list != null) {
			try {
				Attribute a = list.get(TagFromName.ReferencedFileID);
				if (a != null) {
					String[] filePath = a.getStringValues();
					fileName=buildPathFromParentAndStringArray(parentFilePath,filePath);
				}
			}
			catch (DicomException e) {
			}
		}
		String uid = Attribute.getSingleStringValueOrNull(list,TagFromName.ReferencedSOPInstanceUIDInFile);
		if (fileName != null && uid != null) {
//System.err.println("Adding "+uid+" = "+fileName);
			mapOfSOPInstanceUIDToReferencedFileName.put(uid,fileName);
		}
		int n = getChildCount(record);
		for (int i=0; i<n; ++i) addToMapOfSOPInstanceUIDToReferencedFileName((DicomDirectoryRecord)getChild(record,i),parentFilePath);
	}

	/**
	 * @param	parentFilePath
	 */
	public Map getMapOfSOPInstanceUIDToReferencedFileName(String parentFilePath) {
		if (mapOfSOPInstanceUIDToReferencedFileName == null) {
//long startTime = System.currentTimeMillis();
			mapOfSOPInstanceUIDToReferencedFileName = new HashMap();
			addToMapOfSOPInstanceUIDToReferencedFileName(root,parentFilePath);
//long currentTime = System.currentTimeMillis();
//System.err.println("DicomDirectory.getMapOfSOPInstanceUIDToReferencedFileName(): lazy instantiation of mapOfSOPInstanceUIDToReferencedFileName took = "+(currentTime-startTime)+" ms");
		}
		return mapOfSOPInstanceUIDToReferencedFileName;
	}

	// Convenience methods and their supporting methods ...

	/**
	 * @param	sopInstanceUID
	 */
	public String getReferencedFileNameForSOPInstanceUID(String sopInstanceUID) throws DicomException {
		if (mapOfSOPInstanceUIDToReferencedFileName == null) {
			throw new DicomException("Map of SOPInstanceUID to ReferencedFileName has not been initialized");
		}
		else {
			return (String)(mapOfSOPInstanceUIDToReferencedFileName.get(sopInstanceUID));

		}
	}

	/**
	 * @param	parent
	 * @param	components
	 */
	private static String buildPathFromParentAndStringArray(String parent,String[] components) {
		File path = (parent == null) ? null : new File(parent);
		for (int i=0; i<components.length; ++i) path = (path == null) ? new File(components[i]) : new File(path,components[i]);
		return path.getPath();
	}

	/**
	 * @param	record
	 * @param	parentFilePath
	 */
	private static String getReferencedFileName(DicomDirectoryRecord record,String parentFilePath) {
		String name=null;
		AttributeList list = ((DicomDirectoryRecord)record).getAttributeList();
		if (list != null) {
			//System.err.println(list);
			try {
				Attribute a = list.get(TagFromName.ReferencedFileID);
				if (a != null) {
					String[] filePath = a.getStringValues();
					name=buildPathFromParentAndStringArray(parentFilePath,filePath);
				}
			}
			catch (DicomException e) {
			}
		}
		return name;
	}

	/**
	 * <p>Get all the referenced file names at or below the specified directory record, and a map to the directory records that reference them.</p>
	 *
	 * @param	record
	 * @param	parentFilePath		the folder in which the DICOMDIR lives (i.e., the base for contained references)
	 * @return				a java.util.HashMap whose keys are string file names fully qualified by the specified parent, mapped to DicomDirectoryRecords
	 */
	public static HashMap findAllContainedReferencedFileNamesAndTheirRecords(DicomDirectoryRecord record,String parentFilePath) {
		HashMap map = new HashMap();
		String name = getReferencedFileName(record,parentFilePath);
		if (name != null) {
			map.put(name,record);
		}
		int nChildren = record.getChildCount();
		for (int i=0; i<nChildren; ++i) {
			DicomDirectoryRecord child=(DicomDirectoryRecord)(record.getChildAt(i));
			map.putAll(findAllContainedReferencedFileNamesAndTheirRecords(child,parentFilePath));
		}
		return map;
	}


	/**
	 * <p>Get all the referenced file names in the entire directory, and a map to the directory records that reference them.</p>
	 *
	 * @param	parentFilePath		the folder in which the DICOMDIR lives (i.e., the base for contained references)
	 * @return				a java.util.HashMap whose keys are string file names fully qualified by the specified parent, mapped to DicomDirectoryRecords
	 */
	public HashMap findAllContainedReferencedFileNamesAndTheirRecords(String parentFilePath) {
		return findAllContainedReferencedFileNamesAndTheirRecords((DicomDirectoryRecord)(getRoot()),parentFilePath);
	}

	/**
	 * <p>Get all the referenced file names at or below the specified directory record.</p>
	 *
	 * @param	record
	 * @param	parentFilePath		the folder in which the DICOMDIR lives (i.e., the base for contained references)
	 * @return				a java.util.Vector of string file names fully qualified by the specified parent
	 */
	public static Vector findAllContainedReferencedFileNames(DicomDirectoryRecord record,String parentFilePath) {
//long startTime = System.currentTimeMillis();
		Vector names = new Vector();
		//String name = getReferencedFileName(record,parentFilePath);
		//if (name != null) names.add(name);
		//int nChildren = record.getChildCount();
		//for (int i=0; i<nChildren; ++i) {
		//	DicomDirectoryRecord child=(DicomDirectoryRecord)(record.getChildAt(i));
		//	names.addAll(findAllContainedReferencedFileNames(child,parentFilePath));
		//}
		//return names;
		HashMap map = findAllContainedReferencedFileNamesAndTheirRecords(record,parentFilePath);
		names.addAll(map.keySet());
//long currentTime = System.currentTimeMillis();
//System.err.println("DicomDirectory.findAllContainedReferencedFileNames(): took = "+(currentTime-startTime)+" ms");
		return names;
	}

	/**
	 * <p>Get all the referenced file names in the entire directory.</p>
	 *
	 * @param	parentFilePath		the folder in which the DICOMDIR lives (i.e., the base for contained references)
	 * @return				a java.util.Vector of string file names fully qualified by the specified parent
	 */
	public Vector findAllContainedReferencedFileNames(String parentFilePath) {
		return findAllContainedReferencedFileNames((DicomDirectoryRecord)(getRoot()),parentFilePath);
	}


	/**
	 * @param	record
	 * @param	attributeLists
	 * @param	frameOfReferenceUID
	 */
	private static void findAllImagesForFrameOfReference(DicomDirectoryRecord record,Vector attributeLists,String frameOfReferenceUID) {
		if (record != null) {
			AttributeList list=record.getAttributeList();
			if (list != null) {
				if (Attribute.getSingleStringValueOrEmptyString(list,TagFromName.DirectoryRecordType).equals("IMAGE")) {
					if (Attribute.getSingleStringValueOrEmptyString(list,TagFromName.FrameOfReferenceUID).equals(frameOfReferenceUID)) {
						attributeLists.add(list);
					}
				}
			}
			int nChildren = record.getChildCount();
			for (int i=0; i<nChildren; ++i) {
				findAllImagesForFrameOfReference((DicomDirectoryRecord)(record.getChildAt(i)),attributeLists,frameOfReferenceUID);
			}
		}
	}

	/**
	 * <p>Get the attribute lists from all the IMAGE level directory records which have a particular FrameOfReferenceUID.</p>
	 *
	 * <p>Useful for finding potential localizers and orthogonal images.</p>
	 *
	 * <p>Note that even though FrameOfReference is a series level entity, in the CT/MR profiles it is specified at the IMAGE directory record level.</p>
	 *
	 * @param	frameOfReferenceUID
	 * @return				a java.util.Vector of com.pixelmed.dicom.AttributeList
	 */
	public Vector findAllImagesForFrameOfReference(String frameOfReferenceUID) {
//long startTime = System.currentTimeMillis();
		Vector attributeLists = new Vector();
		findAllImagesForFrameOfReference((DicomDirectoryRecord)getRoot(),attributeLists,frameOfReferenceUID);
//long currentTime = System.currentTimeMillis();
//System.err.println("DicomDirectory.findAllImagesForFrameOfReference(): took = "+(currentTime-startTime)+" ms");
		return attributeLists;
	}
}





