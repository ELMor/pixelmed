/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import com.pixelmed.utils.HexDump;

import java.io.*;

/**
 * <p>A class to abstract the contents of a file meta information header as used for a
 * DICOM PS 3.10 file, with additional static methods to add to and extract from an
 * existing list of attributes.</p>
 *
 * @author	dclunie
 */
public class FileMetaInformation {

	private static final String identString = "@(#) $Header$";
	
	private static final AttributeTag groupLengthTag = new AttributeTag(0x0002,0x0000);

	private AttributeList list;
	
	/**
	 * <p>Construct an instance of the  file meta information from the specified parameters.</p>
	 *
	 * @param	mediaStorageSOPClassUID		the SOP Class UID of the dataset to which the file meta information will be prepended
	 * @param	mediaStorageSOPInstanceUID	the SOP Instance UID of the dataset to which the file meta information will be prepended
	 * @param	transferSyntaxUID		the transfer syntax UID that will be used to write the dataset
	 * @param	sourceApplicationEntityTitle	the source AE title of the dataset
	 * @exception	DicomException
	 */
	public FileMetaInformation(String mediaStorageSOPClassUID,String mediaStorageSOPInstanceUID,String transferSyntaxUID,String sourceApplicationEntityTitle) throws DicomException {
		list=new AttributeList();
		addFileMetaInformation(list,mediaStorageSOPClassUID,mediaStorageSOPInstanceUID,transferSyntaxUID,sourceApplicationEntityTitle);
	}
	
	/**
	 * <p>Add the file meta information attributes to an existing list, using
	 * only the parameters supplied.</p>
	 *
	 * <p>Note that the appropriate (mandatory) file meta information group length tag is also computed and added.</p>
	 *
	 * @param	list				the list to be extended with file meta information attributes
	 * @param	mediaStorageSOPClassUID		the SOP Class UID of the dataset to which the file meta information will be prepended
	 * @param	mediaStorageSOPInstanceUID	the SOP Instance UID of the dataset to which the file meta information will be prepended
	 * @param	transferSyntaxUID		the transfer syntax UID that will be used to write the dataset
	 * @param	sourceApplicationEntityTitle	the source AE title of the dataset
	 * @exception	DicomException
	 */
	public static void addFileMetaInformation(AttributeList list,
		String mediaStorageSOPClassUID,String mediaStorageSOPInstanceUID,String transferSyntaxUID,String sourceApplicationEntityTitle) throws DicomException {
		int gl =  6 * (4+2+2)	// 6 fixed EVR short-length-form elements
			+ 1 * (4+4+4);	// 1 fixed EVR long-length-form element (OB)

		{ AttributeTag t = TagFromName.FileMetaInformationVersion;   Attribute a = new OtherByteAttribute(t);         byte[] b=new byte[2]; b[0]=0x00; b[1]=0x01; a.setValues(b); list.put(t,a); gl+=a.getPaddedVL(); }
		{ AttributeTag t = TagFromName.MediaStorageSOPClassUID;      Attribute a = new UniqueIdentifierAttribute(t);  a.addValue(mediaStorageSOPClassUID);                        list.put(t,a); gl+=a.getPaddedVL(); }
		{ AttributeTag t = TagFromName.MediaStorageSOPInstanceUID;   Attribute a = new UniqueIdentifierAttribute(t);  a.addValue(mediaStorageSOPInstanceUID);                     list.put(t,a); gl+=a.getPaddedVL(); }
		{ AttributeTag t = TagFromName.TransferSyntaxUID;            Attribute a = new UniqueIdentifierAttribute(t);  a.addValue(transferSyntaxUID);                              list.put(t,a); gl+=a.getPaddedVL(); }
		{ AttributeTag t = TagFromName.ImplementationClassUID;       Attribute a = new UniqueIdentifierAttribute(t);  a.addValue(VersionAndConstants.implementationClassUID);     list.put(t,a); gl+=a.getPaddedVL(); }
		{ AttributeTag t = TagFromName.ImplementationVersionName;    Attribute a = new ShortStringAttribute(t,null);  a.addValue(VersionAndConstants.implementationVersionName);  list.put(t,a); gl+=a.getPaddedVL(); }
		{ AttributeTag t = TagFromName.SourceApplicationEntityTitle; Attribute a = new ApplicationEntityAttribute(t); a.addValue(sourceApplicationEntityTitle);                   list.put(t,a); gl+=a.getPaddedVL(); }

		{ AttributeTag t = groupLengthTag; Attribute a = new UnsignedLongAttribute(t); a.addValue(gl); list.put(t,a); }
	}
	
	
	/**
	 * <p>Add the file meta information attributes to an existing list, extracting
	 * the known UIDs from that list, and adding the additional parameters supplied.</p>
	 *
	 * @param	list				the list to be extended with file meta information attributes
	 * @param	transferSyntaxUID		the transfer syntax UID that will be used to write this list
	 * @param	sourceApplicationEntityTitle	the source AE title of the dataset in the list
	 * @exception	DicomException
	 */
	public static void addFileMetaInformation(AttributeList list,String transferSyntaxUID,String sourceApplicationEntityTitle) throws DicomException {
		String mediaStorageSOPClassUID = null;
		Attribute aSOPClassUID = list.get(TagFromName.SOPClassUID);
		if (aSOPClassUID != null) {
			mediaStorageSOPClassUID = aSOPClassUID.getSingleStringValueOrNull();
		}
		if (mediaStorageSOPClassUID == null ) {
			throw new DicomException("Could not add File Meta Information - missing or empty SOPClassUID");
		}
		
		String mediaStorageSOPInstanceUID = null;
		Attribute aSOPInstanceUID = list.get(TagFromName.SOPInstanceUID);
		if (aSOPInstanceUID != null) {
			mediaStorageSOPInstanceUID = aSOPInstanceUID.getSingleStringValueOrNull();
		}
		if (mediaStorageSOPInstanceUID == null ) {
			throw new DicomException("Could not add File Meta Information - missing or empty SOPInstanceUID");
		}
		
		addFileMetaInformation(list,mediaStorageSOPClassUID,mediaStorageSOPInstanceUID,transferSyntaxUID,sourceApplicationEntityTitle);
	}

	/**
	 * <p>Get the attribute list in this instance of the file meat information.</p>
	 *
	 * @return	the attribute list
	 */
	public AttributeList getAttributeList() { return list; }
	
	/**
	 * <p>For testing.</p>
	 *
	 * <p>Generate a dummy file meta information header and test reading and writing it.</p>
	 *
	 * @param	arg	ignored
	 */
	public static void main(String arg[]) {

		try {
			AttributeList list = new FileMetaInformation("1.2.3.44","1.2",TransferSyntax.Default,"MYAE").getAttributeList();
			System.err.println("As constructed:");
			System.err.print(list);
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			list.write(new DicomOutputStream(bout,TransferSyntax.ExplicitVRLittleEndian,null));
			byte[] b = bout.toByteArray();
			System.err.print(HexDump.dump(b));
			AttributeList rlist = new AttributeList();
			rlist.read(new DicomInputStream(new ByteArrayInputStream(b),TransferSyntax.ExplicitVRLittleEndian,true));
			System.err.println("As read:");
			System.err.print(rlist);
		}
		catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}
	}
}
