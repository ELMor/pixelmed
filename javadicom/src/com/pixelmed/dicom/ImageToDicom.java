package com.pixelmed.dicom;

import java.io.File;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.SampleModel;

import javax.imageio.ImageIO;

/**
 * <p>A class for converting RGB consumer image input format files (anything JIIO can recognize) into single frame DICOM Secondary Capture images.</p>
 *
 * @author	dclunie
 */

public class ImageToDicom {

	private static final String identString = "@(#) $Header$";

	/**
	 * <p>Read an RGB consumer image input format file (anything JIIO can recognize), and create a single frame DICOM Secondary Capture image.</p>
	 *
	 * @param	inputFile
	 * @param	outputFile
	 * @param	patientName
	 * @param	patientID
	 * @param	studyID
	 * @param	seriesNumber
	 * @param	instanceNumber
	 */
	public ImageToDicom(String inputFile,String outputFile,String patientName,String patientID,String studyID,String seriesNumber,String instanceNumber) {
		try {
			File f = new File(inputFile);
			BufferedImage src = ImageIO.read(f);
			int srcWidth = src.getWidth();
//System.err.println("ImageToDicom.main(): srcWidth = "+srcWidth);
			int srcHeight = src.getHeight();
//System.err.println("ImageToDicom.main(): srcHeight = "+srcHeight);
			
			SampleModel srcSampleModel = src.getSampleModel();
//System.err.println("ImageToDicom.main(): srcSampleModel = "+srcSampleModel);
			int srcDataType = srcSampleModel.getDataType();
//System.err.println("ImageToDicom.main(): srcDataType = "+srcDataType);
			Raster srcRaster = src.getRaster();
			DataBuffer srcDataBuffer = srcRaster.getDataBuffer();
			int srcNumBands = srcRaster.getNumBands();
//System.err.println("ImageToDicom.main(): srcNumBands = "+srcNumBands);
			int srcPixels[] = null; // to disambiguate SampleModel.getPixels() method signature
			srcPixels = srcSampleModel.getPixels(0,0,srcWidth,srcHeight,srcPixels,srcDataBuffer);
			int srcPixelsLength = srcPixels.length;
//System.err.println("ImageToDicom.main(): srcPixelsLength = "+srcPixelsLength);
//System.err.println("ImageToDicom.main(): srcWidth*srcHeight*srcNumBands = "+srcWidth*srcHeight*srcNumBands);

			AttributeList list = new AttributeList();
			
			short rows = (short)srcHeight;
			short columns = (short)srcWidth;
			short numberOfFrames = 1;

			Attribute pixelData = null;
			short bitsAllocated = 0;
			short bitsStored = 0;
			short highBit = 0;
			short samplesPerPixel = 0;
			short pixelRepresentation = 0;
			String photometricInterpretation = null;
			short planarConfiguration = 0;

			int dstPixelsLength = srcWidth*srcHeight*srcNumBands;
			if (srcNumBands == 3 && srcPixelsLength == dstPixelsLength) {
				byte dstPixels[] = new byte[dstPixelsLength];
				int dstIndex=0;
				for (int srcIndex=0; srcIndex<srcPixelsLength;) {
					dstPixels[dstIndex++]=(byte)(srcPixels[srcIndex++]);
					dstPixels[dstIndex++]=(byte)(srcPixels[srcIndex++]);
					dstPixels[dstIndex++]=(byte)(srcPixels[srcIndex++]);
				}
				pixelData = new OtherByteAttribute(TagFromName.PixelData);
				pixelData.setValues(dstPixels);
				bitsAllocated=8;
				bitsStored=8;
				highBit=7;
				samplesPerPixel=3;
				pixelRepresentation=0;
				photometricInterpretation="RGB";
				planarConfiguration=0;	// by pixel
			}
			else {
				throw new Exception("Unsupported pixel data form ("+srcNumBands+" bands)");
			}
			
			if (pixelData != null) {
				list.put(pixelData);

				{ Attribute a = new UnsignedShortAttribute(TagFromName.BitsAllocated); a.addValue(bitsAllocated); list.put(a); }
				{ Attribute a = new UnsignedShortAttribute(TagFromName.BitsStored); a.addValue(bitsStored); list.put(a); }
				{ Attribute a = new UnsignedShortAttribute(TagFromName.HighBit); a.addValue(highBit); list.put(a); }
				{ Attribute a = new UnsignedShortAttribute(TagFromName.Rows); a.addValue(rows); list.put(a); }
				{ Attribute a = new UnsignedShortAttribute(TagFromName.Columns); a.addValue(columns); list.put(a); }
				{ Attribute a = new IntegerStringAttribute(TagFromName.NumberOfFrames); a.addValue(numberOfFrames); list.put(a); }
				{ Attribute a = new UnsignedShortAttribute(TagFromName.SamplesPerPixel); a.addValue(samplesPerPixel); list.put(a); }
				{ Attribute a = new UnsignedShortAttribute(TagFromName.PixelRepresentation); a.addValue(pixelRepresentation); list.put(a); }
				{ Attribute a = new CodeStringAttribute(TagFromName.PhotometricInterpretation); a.addValue(photometricInterpretation); list.put(a); }
				if (samplesPerPixel > 1) {
					Attribute a = new UnsignedShortAttribute(TagFromName.PlanarConfiguration); a.addValue(planarConfiguration); list.put(a);
				}
			
				// various Type 1 and Type 2 attributes for mandatory SC modules ...
	
				UIDGenerator u = new UIDGenerator();	

				{ Attribute a = new UniqueIdentifierAttribute(TagFromName.SOPClassUID); a.addValue(SOPClass.SecondaryCaptureImageStorage); list.put(a); }
				{ Attribute a = new UniqueIdentifierAttribute(TagFromName.SOPInstanceUID); a.addValue(u.getNewSOPInstanceUID(studyID,seriesNumber,instanceNumber)); list.put(a); }
				{ Attribute a = new UniqueIdentifierAttribute(TagFromName.SeriesInstanceUID); a.addValue(u.getNewSeriesInstanceUID(studyID,seriesNumber)); list.put(a); }
				{ Attribute a = new UniqueIdentifierAttribute(TagFromName.StudyInstanceUID); a.addValue(u.getNewStudyInstanceUID(studyID)); list.put(a); }

				{ Attribute a = new PersonNameAttribute(TagFromName.PatientName); a.addValue(patientName); list.put(a); }
				{ Attribute a = new LongStringAttribute(TagFromName.PatientID); a.addValue(patientID); list.put(a); }
				{ Attribute a = new DateAttribute(TagFromName.PatientBirthDate); list.put(a); }
				{ Attribute a = new CodeStringAttribute(TagFromName.PatientSex); list.put(a); }
				{ Attribute a = new ShortStringAttribute(TagFromName.StudyID); a.addValue(studyID); list.put(a); }
				{ Attribute a = new DateAttribute(TagFromName.StudyDate); list.put(a); }
				{ Attribute a = new TimeAttribute(TagFromName.StudyTime); list.put(a); }
				{ Attribute a = new PersonNameAttribute(TagFromName.ReferringPhysicianName); a.addValue("^^^^"); list.put(a); }
				{ Attribute a = new ShortStringAttribute(TagFromName.AccessionNumber); list.put(a); }
				{ Attribute a = new IntegerStringAttribute(TagFromName.SeriesNumber); a.addValue(seriesNumber); list.put(a); }
				{ Attribute a = new IntegerStringAttribute(TagFromName.InstanceNumber); a.addValue(instanceNumber); list.put(a); }
				{ Attribute a = new CodeStringAttribute(TagFromName.Modality); a.addValue("OT"); list.put(a); }
				{ Attribute a = new CodeStringAttribute(TagFromName.ConversionType); a.addValue("WSD"); list.put(a); }
				{ Attribute a = new LongStringAttribute(TagFromName.Manufacturer); list.put(a); }
				{ Attribute a = new CodeStringAttribute(TagFromName.PatientOrientation); list.put(a); }

				FileMetaInformation.addFileMetaInformation(list,TransferSyntax.ExplicitVRLittleEndian,"OURAETITLE");
				list.write(outputFile,TransferSyntax.ExplicitVRLittleEndian,true,true);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * <p>Read an RGB consumer image input format file (anything JIIO can recognize), and create a single frame DICOM Secondary Capture image.</p>
	 *
	 * @param	arg	six parameters, the inputFile, outputFile, patientName, patientID, studyID, seriesNumber, instanceNumber
	 */
	public static void main(String arg[]) {
		new ImageToDicom(arg[0],arg[1],arg[2],arg[3],arg[4],arg[5],arg[6]);
	}
}
