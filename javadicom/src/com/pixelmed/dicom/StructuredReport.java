/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.*;

/**
 * <p>The {@link com.pixelmed.dicom.StructuredReport StructuredReport} class implements a
 * {@link javax.swing.tree.TreeModel TreeModel} to abstract the contents of a list of attributes
 * representing a DICOM Structured Report as
 * a tree in order to provide support for a {@link com.pixelmed.dicom.StructuredReportBrowser StructuredReportBrowser}.</p>
 *
 * <p>For details of some of the methods implemented here see {@link javax.swing.tree.TreeModel javax.swing.tree.TreeModel}.</p>
 *
 * @see com.pixelmed.dicom.ContentItem
 * @see com.pixelmed.dicom.ContentItemFactory
 * @see com.pixelmed.dicom.StructuredReportBrowser
 *
 * @author	dclunie
 */
public class StructuredReport implements TreeModel {

	/***/
	private static final String identString = "@(#) $Header$";

	// Our nodes are all instances of ContentItem ...

	/***/
	private ContentItem root;

	// Stuff to support listener vector

	/***/
	private Vector listeners;

	// Methods for TreeModel

	/**
	 * @param	node
	 * @param	index
	 */
	public Object getChild(Object node,int index) {
		return ((ContentItem)node).getChildAt(index);
	}

	/**
	 * @param	parent
	 * @param	child
	 */
	public int getIndexOfChild(Object parent, Object child) {
		return ((ContentItem)parent).getIndex((ContentItem)child);
	}

	/***/
	public Object getRoot() { return root; }

	/**
	 * @param	parent
	 */
	public int getChildCount(Object parent) {
		return ((ContentItem)parent).getChildCount();
	}

	/**
	 * @param	node
	 */
	public boolean isLeaf(Object node) {
		return ((ContentItem)node).getChildCount() == 0;
	}

	/**
	 * @param	path
	 * @param	newValue
	 */
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	/**
	 * @param	tml
	 */
	public void addTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners = new Vector();
		listeners.addElement(tml);
	}

	/**
	 * @param	tml
	 */
	public void removeTreeModelListener(TreeModelListener tml) {
		if (listeners == null) listeners.removeElement(tml);
	}

	// Methods specific to StructuredReport

	/***/
	private ContentItemFactory nodeFactory;

	/**
	 * @param	parent
	 * @param	list
	 * @exception	DicomException
	 */
	private ContentItem processSubTree(ContentItem parent,AttributeList list) throws DicomException {
//System.err.println("processSubTree:");

		ContentItem node = nodeFactory.getNewContentItem(parent,list);

		if (list != null) {
			SequenceAttribute aContentSequence = (SequenceAttribute)(list.get(TagFromName.ContentSequence));
			if (aContentSequence != null) {
				Iterator i = aContentSequence.iterator();
				while (i.hasNext()) {
					SequenceItem item = ((SequenceItem)i.next());
					node.addChild(processSubTree(node,item == null ? null : item.getAttributeList()));
				}
			}
		}

		return node;
	}

	/**
	 * <p>Construct an internal tree representation of a structured report from
	 * a list of DICOM attributes.</p>
	 *
	 * @param	list		the list of attributes in which the structured report is encoded
	 * @exception	DicomException
	 */
	StructuredReport(AttributeList list) throws DicomException {

//System.err.println(list.toString());

		nodeFactory=new ContentItemFactory();
		root = processSubTree(null,list);
		nodeFactory=null;
	}

	/**
	 * @param	node
	 */
	private String walkTree(ContentItem node) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(node.toString());
		buffer.append("\n");

		int n = getChildCount(node);
		for (int i=0; i<n; ++i) buffer.append(walkTree((ContentItem)getChild(node,i)));

		return buffer.toString();
	}

	/**
	 * <p>Dump the entire tree as a {@link java.lang.String String}.</p>
	 *
	 * @return	a multi-line {@link java.lang.String String} representing the tree
	 */
	public String toString() {
		return walkTree(root);
	}


	// Convenience methods and their supporting methods ...

	/**
	 * <p>Find all coordinate and image references within content items of the sub-tree rooted at the specified node (which may be the root).</p>
	 *
	 * <p>The annotation of the reference is derived from the value of the Concept Name of the parent (and its parent, if a NUM content item).</p>
	 *
	 * @param	item	the node to start searching from
	 * @return		a {@link java.util.Vector Vector} of {@link com.pixelmed.dicom.SpatialCoordinateAndImageReference SpatialCoordinateAndImageReference}
	 */
	public static Vector findAllContainedSOPInstances(ContentItem item) {
		Vector instances = new Vector();
		String uidInstance = item.getReferencedSOPInstanceUID();
		String uidClass = item.getReferencedSOPClassUID();
		if (uidInstance != null) {
			ContentItem parent = item.getParentAsContentItem();
			String graphicType = null;
			float[] graphicData = null;
			String annotation=null;
			if (parent != null) {
				graphicType = parent.getGraphicType();
				graphicData = parent.getGraphicData();
				if (graphicType != null) parent = parent.getParentAsContentItem();	// want enclosing finding, whether scoord or not
				if (parent != null) {
					annotation=parent.getConceptNameAndValue();
					if (parent.getValueType().equals("NUM")) {	// go up one more
						parent = parent.getParentAsContentItem();
						if (parent != null) annotation=parent.getConceptNameAndValue()+" "+annotation;
					}
				}
			}
			instances.add(new SpatialCoordinateAndImageReference(uidInstance,uidClass,graphicType,graphicData,annotation));
		}
		int nChildren = item.getChildCount();
		for (int i=0; i<nChildren; ++i) {
			ContentItem child=(ContentItem)(item.getChildAt(i));
			instances.addAll(findAllContainedSOPInstances(child));
		}
		return instances;
	}

}




