/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.dicom;

import java.util.*;

/**
 * <p>An abstract class of static methods to support removing identifying attributes and adding Clinical Trials Patient, Study and Series Modules attributes.</p>
 *
 * @author	dclunie
 */
abstract public class ClinicalTrialsAttributes {

	/***/
	private static final String identString = "@(#) $Header$";
	
	private static final String defaultValueForMissingNonZeroLengthStrings = "NONE";
	private static final String defaultValueForMissingPossiblyZeroLengthStrings = "";
	
	private ClinicalTrialsAttributes() {};
	
	private static final void addType1LongStringAttribute(AttributeList list,AttributeTag t,String value,SpecificCharacterSet specificCharacterSet) throws DicomException {
		if (value == null || value.length() == 0) value=defaultValueForMissingNonZeroLengthStrings;
		Attribute a = new LongStringAttribute(t,specificCharacterSet);
		a.addValue(value);
		list.put(t,a);
	}

	private static final void addType2LongStringAttribute(AttributeList list,AttributeTag t,String value,SpecificCharacterSet specificCharacterSet) throws DicomException {
		if (value == null) value=defaultValueForMissingPossiblyZeroLengthStrings;
		Attribute a = new LongStringAttribute(t,specificCharacterSet);
		a.addValue(value);
		list.put(t,a);
	}

	private static final void addType3ShortTextAttribute(AttributeList list,AttributeTag t,String value,SpecificCharacterSet specificCharacterSet) throws DicomException {
		if (value != null) {
			Attribute a = new ShortTextAttribute(t,specificCharacterSet);
			a.addValue(value);
			list.put(t,a);
		}
	}

	/**
	 * <p>Add the attributes of the Clinical Trials Patient, Study and Series Modules, to a list of attributes.</p>
	 *
	 * @param	list				the list of attributes to which to add the attributes
	 * @param	replaceConventionalAttributes	if true, use the supplied clinical trials attributes in place of the conventional ID attributes as well
	 * @param	clinicalTrialSponsorName
	 * @param	clinicalTrialProtocolID
	 * @param	clinicalTrialProtocolName
	 * @param	clinicalTrialSiteID
	 * @param	clinicalTrialSiteName
	 * @param	clinicalTrialSubjectID
	 * @param	clinicalTrialSubjectReadingID
	 * @param	clinicalTrialTimePointID
	 * @param	clinicalTrialTimePointDescription
	 * @param	clinicalTrialCoordinatingCenterName
	 * @throws	DicomException
	 */
	public static final void addClinicalTrialsAttributes(AttributeList list,boolean replaceConventionalAttributes,
			String clinicalTrialSponsorName,
			String clinicalTrialProtocolID,
			String clinicalTrialProtocolName,
			String clinicalTrialSiteID,
			String clinicalTrialSiteName,
			String clinicalTrialSubjectID,
			String clinicalTrialSubjectReadingID,
			String clinicalTrialTimePointID,
			String clinicalTrialTimePointDescription,
			String clinicalTrialCoordinatingCenterName) throws DicomException {
			
		Attribute aSpecificCharacterSet = list.get(TagFromName.SpecificCharacterSet);
		SpecificCharacterSet specificCharacterSet = aSpecificCharacterSet == null ? null : new SpecificCharacterSet(aSpecificCharacterSet.getStringValues());
			
		// Clinical Trial Subject Module

		addType1LongStringAttribute(list,TagFromName.ClinicalTrialSponsorName,clinicalTrialSponsorName,specificCharacterSet);
		addType1LongStringAttribute(list,TagFromName.ClinicalTrialProtocolID,clinicalTrialProtocolID,specificCharacterSet);
		addType2LongStringAttribute(list,TagFromName.ClinicalTrialProtocolName,clinicalTrialProtocolName,specificCharacterSet);
		addType2LongStringAttribute(list,TagFromName.ClinicalTrialSiteID,clinicalTrialSiteID,specificCharacterSet);
		addType2LongStringAttribute(list,TagFromName.ClinicalTrialSiteName,clinicalTrialSiteName,specificCharacterSet);
		if (clinicalTrialSubjectID != null || clinicalTrialSubjectReadingID == null)	// must be one or the other present
			addType1LongStringAttribute(list,TagFromName.ClinicalTrialSubjectID,clinicalTrialSubjectID,specificCharacterSet);
		if (clinicalTrialSubjectReadingID != null)
			addType1LongStringAttribute(list,TagFromName.ClinicalTrialSubjectReadingID,clinicalTrialSubjectReadingID,specificCharacterSet);

		// Clinical Trial Study Module

		addType2LongStringAttribute(list,TagFromName.ClinicalTrialTimePointID,clinicalTrialTimePointID,specificCharacterSet);
		addType3ShortTextAttribute(list,TagFromName.ClinicalTrialTimePointDescription,clinicalTrialTimePointDescription,specificCharacterSet);

		// Clinical Trial Series Module

		addType2LongStringAttribute(list,TagFromName.ClinicalTrialCoordinatingCenterName,clinicalTrialCoordinatingCenterName,specificCharacterSet);
		
		if (replaceConventionalAttributes) {
			// Use ClinicalTrialSubjectID to replace both PatientName and PatientID
			{
				String value = clinicalTrialSubjectID;
				if (value == null) value=defaultValueForMissingNonZeroLengthStrings;
				{
					//list.remove(TagFromName.PatientName);
					Attribute a = new PersonNameAttribute(TagFromName.PatientName,specificCharacterSet);
					a.addValue(value);
					list.put(TagFromName.PatientName,a);
				}
				{
					//list.remove(TagFromName.PatientID);
					Attribute a = new LongStringAttribute(TagFromName.PatientID,specificCharacterSet);
					a.addValue(value);
					list.put(TagFromName.PatientID,a);
				}
			}
			// Use ClinicalTrialTimePointID to replace Study ID
			{
				String value = clinicalTrialTimePointID;
				if (value == null) value=defaultValueForMissingNonZeroLengthStrings;
				{
					//list.remove(TagFromName.StudyID);
					Attribute a = new ShortStringAttribute(TagFromName.StudyID,specificCharacterSet);
					a.addValue(value);
					list.put(TagFromName.StudyID,a);
				}
			}
		}
	}

	/**
	 * <p>Deidentify a list of attributes, recursively iterating through nested sequences.</p>
	 *
	 * @param	list		the list of attributes to be cleaned up
	 * @param	keepUIDs	if true, keep the UIDs
	 * @param	keepDescriptors	if true, keep the text description and comment attributes
	 * @throws	DicomException
	 */
	public static final void removeOrNullIdentifyingAttributes(AttributeList list,boolean keepUIDs,boolean keepDescriptors) throws DicomException {
		// use the list from the Basic Application Level Confidentiality Profile in PS 3.15 2003
	
		if (!keepUIDs) {
			list.remove(TagFromName.InstanceCreatorUID);
			list.remove(TagFromName.SOPInstanceUID);
			list.remove(TagFromName.StudyInstanceUID);
			list.remove(TagFromName.SeriesInstanceUID);
			list.remove(TagFromName.FrameOfReferenceUID);
			list.remove(TagFromName.SynchronizationFrameOfReferenceUID);
			list.remove(TagFromName.ReferencedSOPInstanceUID);
			list.remove(TagFromName.UID);
			list.remove(TagFromName.StorageMediaFileSetUID);
			list.remove(TagFromName.ReferencedFrameOfReferenceUID);
			list.remove(TagFromName.RelatedFrameOfReferenceUID);
		}

		if (!keepDescriptors) {
			list.remove(TagFromName.StudyDescription);
			list.remove(TagFromName.SeriesDescription);
		}

		list.replaceWithZeroLengthIfPresent(TagFromName.AccessionNumber);
		list.remove(TagFromName.InstitutionName);
		list.remove(TagFromName.InstitutionAddress);
		list.replaceWithZeroLengthIfPresent(TagFromName.ReferringPhysicianName);
		list.remove(TagFromName.ReferringPhysicianAddress);
		list.remove(TagFromName.ReferringPhysicianTelephoneNumber);
		list.remove(TagFromName.StationName);
		list.remove(TagFromName.InstitutionalDepartmentName);
		list.remove(TagFromName.PhysicianOfRecord);
		list.remove(TagFromName.PerformingPhysicianName);
		list.remove(TagFromName.PhysicianReadingStudy);
		list.remove(TagFromName.OperatorName);
		list.remove(TagFromName.AdmittingDiagnosesDescription);
		list.remove(TagFromName.DerivationDescription);
		list.replaceWithZeroLengthIfPresent(TagFromName.PatientName);
		list.replaceWithZeroLengthIfPresent(TagFromName.PatientID);
		list.replaceWithZeroLengthIfPresent(TagFromName.PatientBirthDate);
		list.remove(TagFromName.PatientBirthTime);
		list.replaceWithZeroLengthIfPresent(TagFromName.PatientSex);
		list.remove(TagFromName.OtherPatientID);
		list.remove(TagFromName.OtherPatientName);
		list.remove(TagFromName.PatientAge);
		list.remove(TagFromName.PatientSize);
		list.remove(TagFromName.PatientWeight);
		list.remove(TagFromName.MedicalRecordLocator);
		list.remove(TagFromName.EthnicGroup);
		list.remove(TagFromName.Occupation);
		list.remove(TagFromName.AdditionalPatientHistory);
		list.remove(TagFromName.PatientComments);
		list.remove(TagFromName.DeviceSerialNumber);
		list.remove(TagFromName.ProtocolName);
		list.replaceWithZeroLengthIfPresent(TagFromName.StudyID);
		list.remove(TagFromName.RequestAttributesSequence);

		if (!keepDescriptors) {
			list.remove(TagFromName.ImageComments);
		}

		// ContentSequence

		// others that it would seem necessary to remove ...
		
		list.remove(TagFromName.ReferencedPatientSequence);
		list.remove(TagFromName.ReferringPhysicianIdentificationSequence);
		list.remove(TagFromName.PhysicianOfRecordIdentificationSequence);
		list.remove(TagFromName.PhysicianReadingStudyIdentificationSequence);
		list.remove(TagFromName.ReferencedStudySequence);
		list.remove(TagFromName.AdmittingDiagnosesCodeSequence);
		list.remove(TagFromName.PerformingPhysicianIdentificationSequence);
		list.remove(TagFromName.OperatorIdentificationSequence);
		list.remove(TagFromName.ReferencedPerformedProcedureStepSequence);
		list.remove(TagFromName.PerformedProcedureStepID);

		if (!keepUIDs) {
			list.remove(TagFromName.ReferencedImageSequence);
			list.remove(TagFromName.SourceImageSequence);
		}

		if (!keepDescriptors) {
			list.remove(TagFromName.PerformedProcedureStepDescription);
			list.remove(TagFromName.CommentsOnPerformedProcedureStep);
			list.remove(TagFromName.AcquisitionComments);
		}
		
		// recursively iterate through any sequences ...
		
		Iterator i = list.values().iterator();
		while (i.hasNext()) {
			Object o = i.next();
			if (o instanceof SequenceAttribute) {
				SequenceAttribute a = (SequenceAttribute)o;
				Iterator items = a.iterator();
				if (items != null) {
					while (items.hasNext()) {
						SequenceItem item = (SequenceItem)(items.next());
						if (item != null) {
							AttributeList itemAttributeList = item.getAttributeList();
							if (itemAttributeList != null) {
								removeOrNullIdentifyingAttributes(itemAttributeList,keepUIDs,keepDescriptors);
							}
						}
					}
				}
			}
		}

	}
	
	/**
	 * <p>For testing.</p>
	 *
	 * <p>Read a DICOM object from the file specified on the command line, and remove identifying attributes, and add sample clinical trials attributes.</p>
	 *
	 * @param	arg
	 */
	public static void main(String arg[]) {

		System.err.println("do it buffered, looking for metaheader, no uid specified");
		try {
			AttributeList list = new AttributeList();
			list.read(arg[0],null,true,true);
			System.err.println("As read ...");
			System.err.print(list.toString());
			
			list.removePrivateAttributes();
			System.err.println("After remove private ...");
			System.err.print(list.toString());
			
			list.removeGroupLengthAttributes();
			System.err.println("After remove group lengths ...");
			System.err.print(list.toString());
			
			list.removeMetaInformationHeaderAttributes();
			System.err.println("After remove meta information header ...");
			System.err.print(list.toString());

			removeOrNullIdentifyingAttributes(list,true/*keepUIDs*/,true/*keepDescriptors)*/);
			System.err.println("After deidentify, keeping descriptions and UIDs ...");
			System.err.print(list.toString());
			
			removeOrNullIdentifyingAttributes(list,true/*keepUIDs*/,false/*keepDescriptors)*/);
			System.err.println("After deidentify, keeping only UIDs ...");
			System.err.print(list.toString());
			
			//removeOrNullIdentifyingAttributes(list,false/*keepUIDs*/,false/*keepDescriptors)*/);
			//System.err.println("After deidentify, removing everything ...");
			//System.err.print(list.toString());
			
			addClinicalTrialsAttributes(list,true/*replaceConventionalAttributes*/,
				"ourSponsorName",
				"ourProtocolID",
				"ourProtocolName",
				"ourSiteID",
				"ourSiteName",
				"ourSubjectID",
				"ourSubjectReadingID",
				"ourTimePointID",
				"ourTimePointDescription",
				"ourCoordinatingCenterName");

			FileMetaInformation.addFileMetaInformation(list,TransferSyntax.ExplicitVRLittleEndian,"OURAETITLE");
			list.write(arg[1],TransferSyntax.ExplicitVRLittleEndian,true,true);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

}

