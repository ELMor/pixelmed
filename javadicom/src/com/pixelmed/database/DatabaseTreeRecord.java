/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.database;

import java.util.*;
import javax.swing.tree.*;

import com.pixelmed.dicom.*;
import com.pixelmed.utils.StringUtilities;

/**
 * <p>Instances of the {@link com.pixelmed.database.DatabaseTreeRecord DatabaseTreeRecord} class represent
 * nodes in a tree of the {@link com.pixelmed.database.DatabaseTreeModel DatabaseTreeModel} class, which in
 * turn is used by the {@link com.pixelmed.database.DatabaseTreeBrowser DatabaseTreeBrowser} class.</p>
 *
 * <p>This class is publically visible primarily so that selection change listeners can be
 * constructed for {@link com.pixelmed.database.DatabaseTreeBrowser DatabaseTreeBrowser}, since
 * the user's selection is returned as a path of {@link com.pixelmed.database.DatabaseTreeRecord DatabaseTreeRecord}
 * instances, which need to be cast accordingly.</p>
 *
 * @author	dclunie
 */
public class DatabaseTreeRecord implements Comparable, TreeNode {

	private static final String identString = "@(#) $Header$";

	DatabaseTreeRecord parent;
	List children;
	InformationEntity ie;
	String localPrimaryKeyValue;
	String localFileNameValue;
	String value;

	/**
	 * <p>Dump the string value of the node.</p>
	 *
	 * @return	the string value of this node
	 */
	public String toString() {
		return value == null ? "" : value;
	}
	
	// Methods to implement Comparable (allows parent to sort)

	/**
	 * <p>Compare nodes based on the lexicographic order of their string values.</p>
	 *
	 * <p>Note that the comparison is more complex than a simple lexicographic comparison
	 * of strings (as described in the definition of {@link java.lang.String#compareTo(String) java.lang.String.compareTo(String)}
	 * but rather accounts for embedded non-zero padded integers. See {@link com.pixelmed.utils.StringUtilities#compareStringsWithEmbeddedNonZeroPaddedIntegers(String,String) com.pixelmed.utils.compareStringsWithEmbeddedNonZeroPaddedIntegers(String,String)}
	 * </p>
	 * @param	o	the {@link com.pixelmed.database.DatabaseTreeRecord DatabaseTreeRecord}
	 *			to compare this {@link com.pixelmed.database.DatabaseTreeRecord DatabaseTreeRecord} against
	 * @return		the value 0 if the argument is equal to this object; a value less than 0 if this object
	 *			is lexicographically less than the argument; and a value greater than 0 if this object
	 *			is lexicographically greater than the argument
	 */
	public int compareTo(Object o) {
		String ovalue = ((DatabaseTreeRecord)o).value;
		//return value.compareTo(ovalue);
		return StringUtilities.compareStringsWithEmbeddedNonZeroPaddedIntegers(value,ovalue);
	}

	/**
	 * @param	o
	 */
	public boolean equals(Object o) {
		return compareTo(o) == 0;
	}

	// Methods to implement TreeNode ...

	/**
	 * <p>Returns the parent node of this node.</p>
	 *
	 * @return	the parent node, or null if the root
	 */
	public TreeNode getParent() {
		return parent;
	}

	/**
	 * <p>Returns the child at the specified index.</p>
	 *
	 * @param	index	the index of the child to be returned, numbered from 0
	 * @return		the child <code>TreeNode</code> at the specified index
	 */
	public TreeNode getChildAt(int index) {
		return (TreeNode)(children.get(index));
	}

	/**
	 * <p>Returns the index of the specified child from amongst this node's children, if present.</p>
	 *
	 * @param	child	the child to search for amongst this node's children
	 * @return		the index of the child, or -1 if not present
	 */
	public int getIndex(TreeNode child) {
//System.err.println("getIndexOfChild: looking for "+child);
		int n=children.size();
		for (int i=0; i<n; ++i) {
			if (children.get(i).equals(child)) {	// expensive comparison ? :(
//System.err.println("getIndexOfChild: found "+child);
				return i;
			}
		}
		return -1;
	}

	/**
	 * <p> Always returns true, since children may always be added.</p>
	 *
	 * @return	always true
	 */
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * <p> Returns true if the receiver is a leaf (has no children).</p>
	 *
	 * @return	true if the receiver is a leaf
	 */
	public boolean isLeaf() {
		return getChildCount() == 0;
	}

	/**
	 * <p>Return the number of children that this node contains.</p>
	 *
	 * @return	the number of children, 0 if none
	 */
	public int getChildCount() {
		return children == null ? 0 : children.size();
	}

	/**
	 * <p>Returns the children of this node as an {@link java.util.Enumeration Enumeration}.</p>
	 *
	 * @return	the children of this node
	 */
	public Enumeration children() {
		return children == null ? null : new Vector(children).elements();
	}

	// Methods specific to this kind of node ...

	/**
	 * <p>Make a new node in a
	 * tree.</p>
	 *
	 * @param	parent			the parent of this node
	 * @param	value			a string value which is used primarily to sort siblings into lexicographic order
	 * @param	ie			the entity in the database information model that the constructed node is an instance of
	 * @param	localPrimaryKeyValue	the local primary key of the database record corresponding to this node
	 * @param	localFileNameValue	the file name that the database record points to (meaningful only for instance (image) level nodes)
	 */
	public DatabaseTreeRecord(DatabaseTreeRecord parent,String value,InformationEntity ie,String localPrimaryKeyValue,String localFileNameValue) {
		this.parent=parent;
		this.value=value;
		this.ie=ie;
		this.localPrimaryKeyValue=localPrimaryKeyValue;
		this.localFileNameValue=localFileNameValue;
	}

	/**
	 * <p>Add a child to this nodes sorted collection of children.</p>
	 *
	 * @param	child	the child node to be added
	 */
	public void addChild(DatabaseTreeRecord child) {
		if (children == null) children=new ArrayList();	// LinkedList seems to be about the same speed
		// Next is from "http://javaalmanac.com/egs/java.util/coll_InsertInList.html?l=rel"
		// and is way faster than children.add(child) followed by Collections.sort(children)
		int index = Collections.binarySearch(children,child);
		if (index < 0) {
			children.add(-index-1,child);
		}
	}

	/**
	 * <p>Add a sibling to this node,
	 * that is add a child to this
	 * node's parent's sorted collection of children.</p>
	 *
	 * @param	sibling		the sibling node to be added
	 * @exception	DicomException	thrown if this node has no parent
	 */
	public void addSibling(DatabaseTreeRecord sibling) throws DicomException {
		if (parent == null) {
			throw new DicomException("Internal error - root node with sibling");
		}
		else {
			parent.addChild(sibling);
		}
	}

	/**
	 * <p>Get the string value of the node which is used for sorting and human-readable rendering.</p>
	 *
	 * @return	the string value of this node
	 */
	public String getValue() { return value; }
	
	/**
	 * <p>Get the information entity that this node represents.</p>
	 *
	 * @return	information entity that this node represents
	 */
	public InformationEntity getInformationEntity() { return ie; }
	
	/**
	 * <p>Get the string value of the local primary key of the database record corresponding to this node.</p>
	 *
	 * @return	the string value of the local primary key
	 */
	public String getLocalPrimaryKeyValue() { return localPrimaryKeyValue; }
	
	/**
	 * <p>Get the file name that the database record points to (meaningful only for instance (image) level nodes).</p>
	 *
	 * @return	the file name
	 */
	public String getLocalFileNameValue() { return localFileNameValue; }
}



