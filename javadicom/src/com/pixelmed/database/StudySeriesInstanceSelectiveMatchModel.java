/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.database;

import com.pixelmed.dicom.*;

import java.sql.*;
import java.util.Iterator;
import java.util.Map;

/**
 * <p>The {@link com.pixelmed.database.StudySeriesInstanceSelectiveMatchModel StudySeriesInstanceSelectiveMatchModel} class
 * supports a minimal DICOM Study/Series/Instance model.</p>
 *
 * <p>Matching of each information entity is performed by all appropriate attributes at
 * that level, not just the instance UIDs alone that are used in {@link com.pixelmed.database.StudySeriesInstanceModel StudySeriesInstanceModel}.</p>
 *
 * <p>Attributes of other DICOM entities than Study, Series and Instance are included at the appropriate lower level entity.</p>
 *
 * @see com.pixelmed.database.StudySeriesInstanceModel
 *
 * @author	dclunie
 */
public class StudySeriesInstanceSelectiveMatchModel extends StudySeriesInstanceModel {

	/***/
	private static final String identString = "@(#) $Header$";

	/**
	 * @param	databaseName
	 * @exception	DicomException
	 */
	public StudySeriesInstanceSelectiveMatchModel(String databaseName) throws DicomException {
		super(databaseName);
	}

	/**
	 * @param	b
	 * @param	list
	 * @param	ie
	 * @exception	DicomException
	 */
	protected void extendStatementStringWithMatchingAttributesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie) throws DicomException {

		// two possibilities ...
		// 1. iterate through whole list of attributes and insist on match for all present for that IE
		// 2. be more selective ... consider match only on "unique key(s)" for a particular level and ignore others
		//
		// adopt the former approach ...

		// also need to escape wildcards and so on, but ignore for now ...

		if      (ie == InformationEntity.STUDY) {
			// no AND since would be no parent reference preceding
			//b.append("STUDY.StudyInstanceUID LIKE ");
			b.append("STUDY.StudyInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.StudyInstanceUID)));
			b.append(" AND ");
			b.append("STUDY.StudyID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.StudyID)));
			b.append(" AND ");
			b.append("STUDY.StudyDate = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.StudyDate)));
			b.append(" AND ");
			b.append("STUDY.StudyTime = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.StudyTime)));
			b.append(" AND ");
			b.append("STUDY.StudyDescription = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.StudyDescription)));
			b.append(" AND ");
			b.append("STUDY.AccessionNumber = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.AccessionNumber)));
			b.append(" AND ");
			b.append("STUDY.PatientName = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.PatientName)));
			b.append(" AND ");
			b.append("STUDY.PatientID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.PatientID)));
			b.append(" AND ");
			b.append("STUDY.PatientBirthDate = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.PatientBirthDate)));
			b.append(" AND ");
			b.append("STUDY.PatientSex = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.PatientSex)));
		}
		else if (ie == InformationEntity.SERIES) {
			b.append(" AND ");
			//b.append("SERIES.SeriesInstanceUID LIKE ");
			b.append("SERIES.SeriesInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SeriesInstanceUID)));
			b.append(" AND ");
			b.append("SERIES.SeriesDate = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SeriesDate)));
			b.append(" AND ");
			b.append("SERIES.SeriesTime = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SeriesTime)));
			b.append(" AND ");
			b.append("SERIES.SeriesNumber = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SeriesNumber)));
			b.append(" AND ");
			b.append("SERIES.SeriesDescription = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SeriesDescription)));
			b.append(" AND ");
			b.append("SERIES.Modality = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.Modality)));
			b.append(" AND ");
			b.append("SERIES.Manufacturer = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.Manufacturer)));
			b.append(" AND ");
			b.append("SERIES.InstitutionName = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.InstitutionName)));
		}
		else if (ie == InformationEntity.INSTANCE) {
			b.append(" AND ");
			//b.append("INSTANCE.SOPInstanceUID LIKE ");
			b.append("INSTANCE.SOPInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SOPInstanceUID)));
			b.append(" AND ");
			b.append("INSTANCE.ContentDate = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.ContentDate)));
			b.append(" AND ");
			b.append("INSTANCE.ContentTime = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.ContentTime)));
			b.append(" AND ");
			b.append("INSTANCE.AcquisitionDate = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.AcquisitionDate)));
			b.append(" AND ");
			b.append("INSTANCE.AcquisitionTime = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.AcquisitionTime)));
			b.append(" AND ");
			b.append("INSTANCE.AcquisitionDateTime = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.AcquisitionDateTime)));
			b.append(" AND ");
			b.append("INSTANCE.InstanceNumber = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.InstanceNumber)));
		}
	}
}

