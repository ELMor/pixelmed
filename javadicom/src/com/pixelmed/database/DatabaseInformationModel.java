/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.database;

import com.pixelmed.dicom.*;

import java.sql.*;
import java.util.*;

/**
 * <p>The {@link com.pixelmed.database.DatabaseInformationModel DatabaseInformationModel} class is an abstract class that contains the core
 * functionality for storing, accessing and maintaining a persistent representation of selected attributes of composite
 * objects.</p>
 *
 * <p>It hides an underlying SQL database implementation that stores the attributes of each
 * entity in the information model in tables.</p>
 *
 * <p>Abstract sub-classes, such as {@link com.pixelmed.database.DicomDatabaseInformationModel DicomDatabaseInformationModel},
 * may refine the type of information model supported, since this base class is fairly generic and not dependent on any
 * particular DICOM information model. Concrete sub-classes define particular models by overriding the methods that
 * define the root entities and the relationship between entities, as well as the unique keys for each particular
 * entity.</p>
 *
 * <p>In addition, the concrete sub-classes define which attributes of each entity will be included in the persistent
 * representation (the database tables), usually by means of a specialized sub-class of {@link com.pixelmed.dicom.DicomDictionary DicomDictionary}
 * which contains the sub-set of relevant attributes and may change their mapping to information entities, such as
 * {@link com.pixelmed.database.DicomDictionaryForPatientStudySeriesConcatenationInstanceModel DicomDictionaryForPatientStudySeriesConcatenationInstanceModel}.</p>
 *
 * <p>For example, an application might instantiate a {@link com.pixelmed.database.PatientStudySeriesConcatenationInstanceModel PatientStudySeriesConcatenationInstanceModel}
 * or a {@link com.pixelmed.database.StudySeriesInstanceModel StudySeriesInstanceModel}, as follows:</p>
 *
 * <pre>
 *	final DatabaseInformationModel d = new PatientStudySeriesConcatenationInstanceModel("test");
 * </pre>
 *
 * <p>Composite objects previously read into a {@link com.pixelmed.dicom.AttributeList AttributeList} from a
 * DICOM file in a persistent location could be inserted into the persistent representation (the database)
 * as follows:
 *
 * <pre>
 *	d.insertObject(list,fileName);
 * </pre>
 *
 * @see com.pixelmed.database
 *
 * @author	dclunie
 */
public abstract class DatabaseInformationModel {

	/***/
	private static final String identString = "@(#) $Header$";

	/***/
	private static final long SQL_INTEGER_MINIMUM = java.lang.Integer.MIN_VALUE;		// the hsqldb INTEGER type is allegedly a Java int
	/***/
	private static final long SQL_INTEGER_MAXIMUM = java.lang.Integer.MAX_VALUE;

	/***/
	protected static final String localPrimaryKeyColumnName = "LocalPrimaryKey";
	/***/
	protected static final String localParentReferenceColumnName = "LocalParentReference";
	/***/
	protected static final String localRecordInsertionTimeColumnName = "RecordInsertionTime";
	/***/
	protected static final String localFileName = "LocalFileName";

	/***/
	protected InformationEntity rootInformationEntity;
	/***/
	protected HashMap listsOfAttributesByInformationEntity;
	/***/
	protected Connection databaseConnection;
	
	/***/
	protected DicomDictionary dictionary;

	/***/
	private Map descriptiveNameMap;

	/***/
	private HashSet localColumnExcludeList;

	/***/
	HashSet getLocalColumnExcludeList() {			// package scope ... used by DatabaseTreeBrowser
		return localColumnExcludeList;
	}

	/**
	 * <p>Return a list of (upper case) column names not to include in human-readable renderings like browsers.</p>
	 * 
	 * <p>Includes such things as the internal primary and parent keys, record insertion time, local file names.</p>
	 */
	/*protected*/ void makeLocalColumnExcludeList() {		// protected scope ... may be (but isn't currently) overridden by specialized classes
		localColumnExcludeList = new HashSet();
		localColumnExcludeList.add(localPrimaryKeyColumnName.toUpperCase());
		localColumnExcludeList.add(localParentReferenceColumnName.toUpperCase());
		localColumnExcludeList.add(localRecordInsertionTimeColumnName.toUpperCase());
		localColumnExcludeList.add(localFileName.toUpperCase());
	}

	/**
	 * <p>Instantiate a persistent information model using the named database.</p>
	 *
	 * <p>Will open a connection to the database, and create any tables if this is the first time,
	 * or re-use any persistent information if not.</p>
	 *
	 * @param	databaseName		the name of the underlying SQL database instance to be used
	 * @param	rootInformationEntity	the top entity of the information model; specific to a particular model's constructor
	 * @param	dictionary		used to decide which attributes to include for each entity when creating the tables
	 * @exception	DicomException		thrown if a connection to the database cannot be established
	 */
	public DatabaseInformationModel(String databaseName,InformationEntity rootInformationEntity,DicomDictionary dictionary) throws DicomException {
		this.rootInformationEntity=rootInformationEntity;
		this.dictionary=dictionary;
		if (this.dictionary == null) this.dictionary = new DicomDictionary();
		listsOfAttributesByInformationEntity = new HashMap(6);
		makeLocalColumnExcludeList();
		try {
			Class.forName("org.hsqldb.jdbcDriver").newInstance();
			databaseConnection=DriverManager.getConnection("jdbc:hsqldb:"+databaseName,"sa","");
//System.err.println("DatabaseInformationModel(): first call to primeListsOfAttributesByInformationEntityFromExistingMetaData() to see if tables exist");
			primeListsOfAttributesByInformationEntityFromExistingMetaData();
			if (listsOfAttributesByInformationEntity.size() == 0) {
//System.err.println("DatabaseInformationModel(): our tables do not exist, create them");
				createTables();
//System.err.println("DatabaseInformationModel(): second call to primeListsOfAttributesByInformationEntityFromExistingMetaData() now that we have added our tables");
				primeListsOfAttributesByInformationEntityFromExistingMetaData();
			}
			createDescriptiveNameMap();
		}
		catch (Exception e) {
			throw new DicomException("Cannot connect to database: "+e);
		}
	}

	/**
	 * <p>Close the underlying connection to the database.</p>
	 *
	 * <p>Prior to actually closing will also try to formally shutdown and compact the database.</p>
	 */
	public void close() {
		if (databaseConnection != null) {
//System.err.println("DatabaseInformationModel.close(): shutdown compact start");
			try {
				Statement s = databaseConnection.createStatement();
				s.execute("SHUTDOWN COMPACT;");
				s.close();
			} catch (SQLException e) {
				e.printStackTrace(System.err);
			}
//System.err.println("DatabaseInformationModel.close(): shutdown compact finished");
			try {
				databaseConnection.close();
			}
			catch (SQLException e) {
				e.printStackTrace(System.err);
			}
			databaseConnection=null;
		}
	}

	/***/
	protected void finalize() {
		close();
	}
	
	/**
	 * <p>Get the entity that is at the top of the information model.</p>
	 *
	 * <p>May be used, for example, to begin top-down traversal of a composite object, or the entire database.</p>
	 *
	 * <p>Will be specific to a particular concrete information model.</p>
	 *
	 * @return	the {@link com.pixelmed.dicom.InformationEntity InformationEntity} at the top of the information model
	 */
	InformationEntity getRootInformationEntity() { return rootInformationEntity; }

	/**
	 * <p>Is the entity in the information model ? </p>
	 *
	 * <p>Will be specific to a particular concrete information model.</p>
	 *
	 * @return	boolean		the information entity is in the information model
	 */
	protected abstract boolean isInformationEntityInModel(InformationEntity ie);

	/**
	 * <p>Is the named entity in the information model ? </p>
	 *
	 * <p>Will be specific to a particular concrete information model.</p>
	 *
	 * @param	ieName		a String name, whose case is ignored (e.g., a database table named for the corresponding information entity)
	 * @return			the name corresponds to an information entity that is in the information model
	 */
	protected boolean isInformationEntityInModel(String ieName) {
		InformationEntity ie = InformationEntity.fromString(ieName);
		return ie == null ? false : isInformationEntityInModel(ie);
	}

	/**
	 * <p>Get the map of attribute (table column) names to descriptive names.</p>
	 *
	 * <p>May be used, for example, for column header labels in table browsers.</p>
	 *
	 * @return	a {@link java.util.HashMap HashMap} indexed by {@link java.lang.String String} column names and containing {@link java.lang.String String} descriptive names
	 */
	Map getDescriptiveNameMap() { return descriptiveNameMap; }

	/**
	 * @exception	DicomException
	 */
	private void createDescriptiveNameMap() throws DicomException {
		descriptiveNameMap = new HashMap();
		Iterator i = dictionary.getTagIterator();
		while (i.hasNext()) {
			AttributeTag tag = (AttributeTag)i.next();
			String descriptiveName = dictionary.getNameFromTag(tag);
			if (descriptiveName != null) {
				String columnName = descriptiveName.toUpperCase();
				descriptiveNameMap.put(columnName,descriptiveName);
			}
		}
	}

	/**
	 * @exception	DicomException
	 */
	private void createTables() throws DicomException {
		InformationEntity ie = rootInformationEntity;
		while (ie != null) {
			createTable(ie,ie != rootInformationEntity);
			ie=getChildTypeForParent(ie);
		}
	}

	/**
	 * @param	ie
	 * @param	withParentReference
	 * @exception	DicomException
	 */
	private void createTable(InformationEntity ie,boolean withParentReference) throws DicomException {
		String tableName = ie.toString().toUpperCase();
		if (tableName == null) {
			throw new DicomException("Internal error: Cannot get name for table from "+ie);
		}
		try {
			StringBuffer b = new StringBuffer();
			b.append("CREATE CACHED TABLE ");
			//b.append("CREATE TABLE ");
			b.append(tableName);
			b.append(" (");
			extendCreateStatementStringWithMandatoryColumns(b,withParentReference,ie);
			extendCreateStatementStringWithAttributesInDicomDictionary(b,ie);
			extendCreateStatementStringWithAnyExtraAttributes(b,ie);
			b.append(")");
			Statement s = databaseConnection.createStatement();
			s.execute(b.toString());
			s.close();
		} catch (Exception e) {
			throw new DicomException("Cannot create table "+tableName+" in database: "+e);
		}
		// listsOfAttributesByInformationEntity is built later ...
		
		if (withParentReference) {
			try {
				StringBuffer b = new StringBuffer();
				b.append("CREATE INDEX ");
				b.append(tableName);
				b.append("_PREFIDX ON ");
				b.append(tableName);
				b.append(" (");
				b.append(localParentReferenceColumnName);
				b.append(")");
				Statement s = databaseConnection.createStatement();
				s.execute(b.toString());
				s.close();
			} catch (Exception e) {
				throw new DicomException("Cannot create index of parents for "+tableName+" in database: "+e);
			}
		}
	}

	/**
	 * @param	b
	 * @param	withParentReference
	 * @param	ie
	 */
	private void extendCreateStatementStringWithMandatoryColumns(StringBuffer b,boolean withParentReference,InformationEntity ie) {
		b.append(localPrimaryKeyColumnName);
		b.append(" CHAR(64)");
		b.append(" PRIMARY KEY");
		if (withParentReference) {
			b.append(", ");
			b.append(localParentReferenceColumnName);
			b.append(" CHAR(64)");
		}
		b.append(",");
		b.append(localRecordInsertionTimeColumnName);
		b.append(" BIGINT");				// not INTEGER, since in hsqldb INTEGER is int, BIGINT is long
		if (ie == InformationEntity.INSTANCE) {
			b.append(",");
			b.append(localFileName);
			b.append(" VARCHAR(255)");
		}
	}

	/**
	 * @param	b
	 * @param	ie
	 */
	private void extendCreateStatementStringWithAttributesInDicomDictionary(StringBuffer b,InformationEntity ie) {
		Iterator i = dictionary.getTagIterator();
		while (i.hasNext()) {
			AttributeTag tag = (AttributeTag)i.next();
			if (ie == dictionary.getInformationEntityFromTag(tag)) {
				String tableName = ie.toString().toUpperCase();
				String columnName = dictionary.getNameFromTag(tag);
				String columnType = getSQLTypeFromDicomValueRepresentation(dictionary.getValueRepresentationFromTag(tag));
				if (columnName != null && columnType != null) {
//System.err.println(columnName+" = "+tableName+" "+columnType+" "+isAttributeUsedInTable(tableName,columnName));
					b.append(", ");
					b.append(columnName);
					b.append(" ");
					b.append(columnType);
				}
			}
		}
	}

	/**
	 * <p>Extend a SQL CREATE TABLE statement in the process of being constructed with any additional attributes (columns) that the model requires.</p>
	 *
	 * <p>Called when creating the tables for a new database.</p>
	 *
	 * <p>Specific to each concrete information model extending {@link com.pixelmed.database.DatabaseInformationModel DatabaseInformationModel}.
	 * Defaults to adding no extra columns if not overridden (i.e. it is not abstract).</p>
	 *
	 * <p> For example, there may be a DICOM attribute that is defined to be in a particular information
	 * entity in the dictionary (for example InstanceNumber is at the concatenation level), but for
	 * the convenience of the user of tree and table browsers it may be nice to replicate it into
	 * the tables for lower levels of the information model as well; hence this method might add
	 * InstanceNumber at the instance level. Once created, such attributes will automatically be
	 * included during database inserts. See also
	 * {@link com.pixelmed.database.PatientStudySeriesConcatenationInstanceModel PatientStudySeriesConcatenationInstanceModel}
	 * for example.</p>
	 *
	 * @param	b		the statement being constructed
	 * @param	ie		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} for which a create table statement is being constructed
	 */
	protected void extendCreateStatementStringWithAnyExtraAttributes(StringBuffer b,InformationEntity ie) {}

	/**
	 * @param	list
	 * @exception	DicomException
	 */
	private void extendTablesAsNecessary(AttributeList list) throws DicomException {		// doesn't work with Hypersonic ... ALTER command not supported :(
		DicomDictionary dictionary = list.getDictionary();
		Iterator i = list.values().iterator();
		while (i.hasNext()) {
			Attribute a = (Attribute)i.next();
			AttributeTag tag = a.getTag();
			InformationEntity ie = dictionary.getInformationEntityFromTag(tag);
			if (ie != null) {
				String tableName = ie.toString();
				String columnName = dictionary.getNameFromTag(tag);
				String columnType = getSQLTypeFromDicomValueRepresentation(a.getVR());	// use actual, not dictionary VR, in case was explicit
				if (columnName != null && columnType != null && !isAttributeUsedInTable(tableName,columnName)) {
//System.err.println(a.toString()+" "+columnName+" = "+tableName+" "+columnType+" "+isAttributeUsedInTable(tableName,columnName));
					try {
						Statement s = databaseConnection.createStatement();
						s.execute("ALTER TABLE " + tableName
							+ " ADD COLUMN " + columnName
							+ " " + columnType
							);
						s.close();
					} catch (Exception e) {
						throw new DicomException("Cannot add column "+columnName+" to table "+tableName+" in database: "+e);
					}
				}
			}
		}
	}

	/**
	 * <p>Insert a DICOM composite object, and the relevant attributes of all the entities it contains, into the database.</p>
	 *
	 * <p>Such a composite object may contain information about the patient, study, series and
	 * instance and so on, and for each of these entities the appropriate records will be
	 * created in the appropriate tables</p>
	 *
	 * <p>If records for any entities already exist (as they will often do when inserting multiple
	 * objects for the same patient or study, for example), they are matched based on the unique key for
	 * the appropriate level. They are not replaced and they are not updated, even if some
	 * of the attributes for that entity (other than the unique key) are different or
	 * additional. Even at the instance level (e.g. if an object with the same <code>SOPInstanceUID</code>
	 * is received), the first record will not be overwritten. Whether or not the application calling this method
	 * will have overwritten the corresponding file in the supplied argument or not is outside the
	 * scope of this class.</p>
	 *
	 * <p>The information is taken from the supplied {@link com.pixelmed.dicom.AttributeList AttributeList},
	 * which is presumed to have already been read from a file or obtained through some other means. The supplied
	 * filename is only used to fill in the appropriate instance level attribute.</p>
	 *
	 * @param	list		the DICOM attributes of a composite object, containing the attributes describing this instance of the entity
	 * @param	fileName	the name of a file where the object is stored and from whence it may later be read
	 * @exception	DicomException	thrown if there are problems extracting the DICOM attributes
	 */
	public void insertObject(AttributeList list,String fileName) throws DicomException {
		// iterate through information entities, extracting matching keys, checking for a match, inserting new if not ...
		try {
			InformationEntity ie = rootInformationEntity;
			String localParentReference = null;
			while (ie != null) {
				String tableName = ie.toString().toUpperCase();
				StringBuffer b = new StringBuffer();
				b.append("SELECT ");
				b.append(localPrimaryKeyColumnName);
				b.append(" FROM ");
				b.append(tableName);
				b.append(" WHERE ");
				if (ie != rootInformationEntity) {
					b.append(tableName);
					b.append(".");
					b.append(localParentReferenceColumnName);
					//b.append(" LIKE \'");
					b.append(" = \'");
					b.append(localParentReference);
					b.append("\'");
				}
				extendStatementStringWithMatchingAttributesForSelectedInformationEntity(b,list,ie);

				Statement s = databaseConnection.createStatement();
				ResultSet r = s.executeQuery(b.toString());

				String entityPrimaryKey = null;
				int count = 0;
				while (r.next()) {
					entityPrimaryKey=r.getString(localPrimaryKeyColumnName);
					++count; 
				}
//System.err.println("count="+count+" entityPrimaryKey="+entityPrimaryKey);
				if (count != 1 || entityPrimaryKey == null) {	// too few or too many ... make a new entry ...
//System.err.println("Inserting new row in "+tableName);
					b = new StringBuffer();
					b.append("INSERT INTO ");
					b.append(tableName);
					b.append(" (");
					b.append(localPrimaryKeyColumnName);
					if (ie != rootInformationEntity) {
						b.append(",");
						b.append(localParentReferenceColumnName);
					}
					b.append(",");
					b.append(localRecordInsertionTimeColumnName);
					extendInsertStatementStringWithAttributeNamesForSelectedInformationEntity(b,list,ie);
					b.append(") VALUES (\'");
					entityPrimaryKey=createPrimaryKeyForSelectedInformationEntity(ie);
					b.append(entityPrimaryKey);
					b.append("\'");
					if (ie != rootInformationEntity) {
						b.append(",\'");
						b.append(localParentReference);
						b.append("\'");
					}
					b.append(",");
					b.append(Long.toString(System.currentTimeMillis()));	// no quotes, since INTEGER
					extendInsertStatementStringWithAttributeValuesForSelectedInformationEntity(b,list,ie,fileName);
					b.append(")");
					String ss = b.toString();
//System.err.println(ss);
					s.execute(ss);
				}

				s.close();
//System.err.println("Done "+tableName+" entityPrimaryKey="+entityPrimaryKey+" localParentReference="+localParentReference);
				localParentReference=entityPrimaryKey;
				ie=getChildTypeForParent(ie,list);
			}
		} catch (Exception e) {
                        e.printStackTrace(System.err);
			throw new DicomException("Cannot perform selection: "+e);
		}
	}

	/**
	 * @param	b
	 */
	private void extendStatementStringWithListOfAllTables(StringBuffer b) {
		InformationEntity ie = rootInformationEntity;
		while (ie != null) {
			if (ie != rootInformationEntity) b.append(",");
			String tableName = ie.toString().toUpperCase();
			b.append(tableName);
			ie=getChildTypeForParent(ie);
		}
	}

	/**
	 * <p>Extend a SQL SELECT statement in the process of being constructed with matching clauses for the unique keys of the entity and all its parents.</p>
	 *
	 * <p>For example, a model might specify the unique key for the patient to be <code>PatientID</code>,
	 * the study to be <code>StudyInstanceUID</code>, the series to be <code>SeriesInstanceUID</code> and so on. A
	 * match requested at the series level would then require <code>PatientID</code>, <code>StudyInstanceUID</code>
	 * and <code>SeriesInstanceUID</code> to match what was in the supplied {@link com.pixelmed.dicom.AttributeList AttributeList}</p>
	 *
	 * <p>Called when inserting a new record for an instance of the entity.</p>
	 *
	 * <p>Specific to each concrete information model extending {@link com.pixelmed.database.DatabaseInformationModel DatabaseInformationModel}.</p>
	 *
	 * @param	b		the statement being constructed
	 * @param	list		the DICOM attributes of a composite object, containing the attributes describing this instance of the entity
	 * @param	ie		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} for which a select statement is being constructed
	 * @exception	DicomException	thrown if there are problems extracting the DICOM attributes
	 */
	protected abstract void extendStatementStringWithMatchingAttributesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie) throws DicomException;

	/**
	 * <p>Extend a SQL INSERT statement in the process of being constructed with the names of the attributes in the instance for the entity.</p>
	 *
	 * <p>Called when inserting a new record for an instance of the entity.</p>
	 *
	 * <p>Specific to each concrete information model extending {@link com.pixelmed.database.DatabaseInformationModel DatabaseInformationModel}.</p>
	 *
	 * @param	b		the statement being constructed
	 * @param	list		the DICOM attributes of a composite object, containing the attributes describing this instance of the entity
	 * @param	ie		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} for which an insert statement is being constructed
	 * @exception	DicomException	thrown if there are problems extracting the DICOM attributes
	 */
	protected abstract void extendInsertStatementStringWithAttributeNamesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie) throws DicomException;

	/**
	 * <p>Extend a SQL INSERT statement in the process of being constructed with the values of the attributes in the instance for the entity.</p>
	 *
	 * <p>Called when inserting a new record for an instance of the entity.</p>
	 *
	 * <p>Specific to each concrete information model extending {@link com.pixelmed.database.DatabaseInformationModel DatabaseInformationModel}.</p>
	 *
	 * @param	b		the statement being constructed
	 * @param	list		the DICOM attributes of a composite object, containing the attributes describing this instance of the entity
	 * @param	ie		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} for which an insert statement is being constructed
	 * @param	fileName	the local filename, which may be non-null for <code>INSTANCE</code> level insertions
	 * @exception	DicomException	thrown if there are problems extracting the DICOM attributes
	 */
	protected abstract void extendInsertStatementStringWithAttributeValuesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie,String fileName) throws DicomException;
	
	/**
	 * <p>Create a new unique key which may be used to identify a new instance of an entity.</p>
	 *
	 * <p>Called when inserting a new record for an instance of the entity.</p>
	 *
	 * @param	ie	the {@link com.pixelmed.dicom.InformationEntity InformationEntity} for which a key is requested
	 * @return		string value of a unique key
	 */
	protected abstract String createPrimaryKeyForSelectedInformationEntity(InformationEntity ie);

	/**
	 * @exception	SQLException
	 */
	private void primeListsOfAttributesByInformationEntityFromExistingMetaData() throws SQLException {
		DatabaseMetaData meta = databaseConnection.getMetaData();
		ResultSet columns = meta.getColumns(null, null, null, null);
		while (columns.next()) {
			String tableName  = columns.getString(3);
			String columnName = columns.getString(4);
			//String type = columns.getString(6);
//System.err.println("DatabaseInformationModel.primeListsOfAttributesByInformationEntityFromExistingMetaData(): tableName="+tableName+" columnName="+columnName);
			if (tableName  != null && columnName != null && isInformationEntityInModel(tableName)) {
//System.err.println("DatabaseInformationModel.primeListsOfAttributesByInformationEntityFromExistingMetaData(): non-null and tableName is in the model");
				LinkedList listOfAttributes = (LinkedList)listsOfAttributesByInformationEntity.get(tableName);
				if (listOfAttributes == null) {
					listOfAttributes = new LinkedList();
					listsOfAttributesByInformationEntity.put(tableName,listOfAttributes);	// NB. column name will be all UPPERCASE
				}
//System.err.println("DatabaseInformationModel.primeListsOfAttributesByInformationEntityFromExistingMetaData(): adding to table "+tableName+" "+columnName);
				listOfAttributes.add(columnName);
			}
		}
		columns.close();
	}

	/**
	 * <p>Is the specified attribute (column) recorded in the specified entity?</p>
	 *
	 * @param	ie		the {@link com.pixelmed.dicom.InformationEntity InformationEntity}
	 * @param	columnName	the string name of the attribute (the column name) (case insensitive)
	 * @return			true if the attribute is used in the table
	 */
	protected boolean isAttributeUsedInTable(InformationEntity ie,String columnName) {
		return ie == null ? false : isAttributeUsedInTable(ie.toString().toUpperCase(),columnName);
	}

	/**
	 * <p>Is the specified attribute (column) recorded in the specified table?</p>
	 *
	 * @param	tableName	the string name of the table (case insensitive)
	 * @param	columnName	the string name of the attribute (the column name) (case insensitive)
	 * @return			true if the attribute is used in the table
	 */
	protected boolean isAttributeUsedInTable(String tableName,String columnName) {
		if (listsOfAttributesByInformationEntity != null) {
			LinkedList listOfAttributes = (LinkedList)listsOfAttributesByInformationEntity.get(tableName);
			if (listOfAttributes != null) {
				if (listOfAttributes.contains(columnName.toUpperCase())) {
//System.err.println("DatabaseInformationModel.isAttributeUsedInTable(String,String): "+tableName+" contains "+columnName);
					return true;	// NB. depends on all identical strings being same object
				}
				else {
//System.err.println("DatabaseInformationModel.isAttributeUsedInTable(String,String): "+tableName+" does not contain "+columnName);
				}
			}
		}
		return false;
	}

	/**
	 * <p>For a particular instance of an information entity, update the record in the database table with a new value for the specified attribute (column).</p>
	 *
	 * <p>For example, for the study entity, this would update an attribute of a particular study, for example the <code>StudyID</code> attribute.</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity} of the record to be updated
	 * @param	localPrimaryKeyValue	the string value of the unique key which identifies the instance of the entity (not including wildcards)
	 * @param	key			the string name of the attribute (column) to be set (updated)
	 * @param	value			the string value to set
	 * @exception	DicomException		thrown if the update fails
	 */
	public void updateSelectedRecord(InformationEntity ie,String localPrimaryKeyValue,String key,String value) throws DicomException {
//System.err.println("updateSelectedRecord: "+ie+" "+localPrimaryKeyValue+" "+key+" "+value);
		try {
			if (ie != null) {
				String tableName = ie.toString();
				StringBuffer b = new StringBuffer();
				b.append("UPDATE ");
				b.append(tableName);
				if (key != null) {
					b.append(" SET ");
					b.append(key);
					b.append(" = \'");
					b.append(value);
					b.append("\'");
				}
				if (localPrimaryKeyValue != null) {
					b.append(" WHERE ");
					b.append(localPrimaryKeyColumnName);
					//b.append(" LIKE \'");
					b.append(" = \'");
					b.append(localPrimaryKeyValue);
					b.append("\'");
				}
				b.append(";");
				Statement s = databaseConnection.createStatement();
				ResultSet r = s.executeQuery(b.toString());
				s.close();
			}
		} catch (Exception e) {
                        e.printStackTrace(System.err);
			throw new DicomException("Cannot perform update: "+e);
		}
	}

	/**
	 * <p>For a particular instance of an information entity, delete the record from the database table.</p>
	 *
	 * <p>For example, for the study entity, this would delete a particular study.</p>
	 *
	 * <p>Note that this method does not delete any referenced files, for example in the case of an instance record.</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity} of the record to be returned
	 * @param	localPrimaryKeyValue	the string value of the unique key which identifies the instance of the entity (not including wildcards)
	 * @exception	DicomException		thrown if the deletion fails
	 */
	public void deleteSelectedRecord(InformationEntity ie,String localPrimaryKeyValue) throws DicomException {
		try {
			if (ie != null) {
				String tableName = ie.toString();
				StringBuffer b = new StringBuffer();
				b.append("DELETE FROM ");
				b.append(tableName);
				if (localPrimaryKeyValue != null) {
					b.append(" WHERE ");
					b.append(localPrimaryKeyColumnName);
					//b.append(" LIKE \'");
					b.append(" = \'");
					b.append(localPrimaryKeyValue);
					b.append("\'");
				}
				b.append(";");
				Statement s = databaseConnection.createStatement();
				ResultSet r = s.executeQuery(b.toString());
				s.close();
			}
		} catch (Exception e) {
                        e.printStackTrace(System.err);
			throw new DicomException("Cannot perform deletion: "+e);
		}
	}

	/**
	 * <p>For a particular instance of an information entity, get the values of all the columns in the entity's database table.</p>
	 *
	 * <p>For example, for the study entity, this would return the attributes for a particular study.</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity} of the record to be returned
	 * @param	localPrimaryKeyValue	the string value of the unique key which identifies the instance of the entity (not including wildcards)
	 * @return				a {@link java.util.TreeMap TreeMap} of {@link java.lang.String String} values indexed by {@link java.lang.String String} column names
	 * @exception	DicomException		thrown if the query fails
	 */
	public Map findAllAttributeValuesForSelectedRecord(InformationEntity ie,String localPrimaryKeyValue) throws DicomException {
		TreeMap map = new TreeMap();
		try {
			if (ie != null) {
				String tableName = ie.toString();
				StringBuffer b = new StringBuffer();
				b.append("SELECT * FROM ");
				b.append(tableName);
				if (localPrimaryKeyValue != null) {
					b.append(" WHERE ");
					b.append(localPrimaryKeyColumnName);
					//b.append(" LIKE \'");
					b.append(" = \'");
					b.append(localPrimaryKeyValue);
					b.append("\'");
				}
				b.append(";");
				Statement s = databaseConnection.createStatement();
				ResultSet r = s.executeQuery(b.toString());
				ResultSetMetaData md = r.getMetaData();
				int numberOfColumns = md.getColumnCount();
				if (r.next()) {							// there should be exactly one
					for (int i=1; i<=numberOfColumns; ++i) {
						String key = md.getColumnName(i);
						String value = r.getString(i);
//System.err.println("findAllAttributeValuesForSelectedRecord: ["+i+"] key = "+key+" value = "+value);
						map.put(key,value);
					}
				}
				s.close();
			}
		} catch (Exception e) {
                        e.printStackTrace(System.err);
			throw new DicomException("Cannot perform selection: "+e);
		}
		return map;
	}

	/**
	 * <p>For all records of an information entity, get the values of all the columns in the entity's database table.</p>
	 *
	 * <p>For example, for the study entity, this would return the attributes for all the studies in the database.</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity} of the records to be returned
	 * @return				an {@link java.util.ArrayList ArrayList} of records, each value of which is a
	 *					{@link java.util.TreeMap TreeMap} of {@link java.lang.String String} values indexed by {@link java.lang.String String} column names
	 * @exception	DicomException		thrown if the query fails
	 */
	public ArrayList findAllAttributeValuesForAllRecordsForThisInformationEntity(InformationEntity ie) throws DicomException {
//long startTime=System.currentTimeMillis();
		ArrayList recordsAsMapsOfStrings = new ArrayList();
		try {
			if (ie != null) {
				String tableName = ie.toString().toUpperCase();
				StringBuffer b = new StringBuffer();
				b.append("SELECT * FROM ");
				b.append(tableName);
				b.append(";");
				Statement s = databaseConnection.createStatement();
				ResultSet r = s.executeQuery(b.toString());
				ResultSetMetaData md = r.getMetaData();
				int numberOfColumns = md.getColumnCount();
				while (r.next()) {
					TreeMap map = new TreeMap();
					for (int i=1; i<=numberOfColumns; ++i) {
						String key = md.getColumnName(i);
						String value = r.getString(i);
//System.err.println("findAllAttributeValuesForAllRecordsForThisInformationEntity: ["+i+"] key = "+key+" value = "+value);
						map.put(key,value);
					}
					recordsAsMapsOfStrings.add(map);
				}
				s.close();
			}
		} catch (Exception e) {
                        e.printStackTrace(System.err);
			throw new DicomException("Cannot perform selection: "+e);
		}
//System.err.println("Select all values for all records of "+ie.toString()+" time "+(System.currentTimeMillis()-startTime)+" milliseconds");
		return recordsAsMapsOfStrings;
	}
	
	private HashMap mapOfInformationEntitiesToColumnNames;
	
	private final String[] getArrayOfColumnNamesForSpecifiedInformationEntity(InformationEntity ie,ResultSet r) throws java.sql.SQLException {
		if (mapOfInformationEntitiesToColumnNames == null) {
			mapOfInformationEntitiesToColumnNames = new HashMap();
		}
		String[] columnNames = (String[])(mapOfInformationEntitiesToColumnNames.get(ie));
		if (columnNames == null) {
//System.err.println("getArrayOfColumnNamesForSpecifiedInformationEntity() "+ie.toString()+" caching metadata");
			ResultSetMetaData md = r.getMetaData();
			int numberOfColumns = md.getColumnCount();
			ArrayList list = new ArrayList();
			for (int i=1; i<=numberOfColumns; ++i) {
				String columName = md.getColumnName(i);
				list.add(columName);
			}
			columnNames=new String[numberOfColumns];
			columnNames=(String[])(list.toArray(columnNames));
			mapOfInformationEntitiesToColumnNames.put(ie,columnNames);
		}
		return columnNames;
	}

	/**
	 * <p>For all records of an information entity with the specified parent, get the values of all the columns in the entity's database table.</p>
	 *
	 * <p>For example, for the series entity, this would return the attributes for all the series of a particular study (parent).</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity} of the records to be returned
	 * @param	localParentReference	the string value of the unique key which identifies the instance of the parent entity (not including wildcards)
	 * @return				an {@link java.util.ArrayList ArrayList} of records, each value of which is a
	 *					{@link java.util.TreeMap TreeMap} of {@link java.lang.String String} values indexed by {@link java.lang.String String} column names
	 * @exception	DicomException		thrown if the query fails
	 */
	public ArrayList findAllAttributeValuesForAllRecordsForThisInformationEntityWithSpecifiedParent(InformationEntity ie,String localParentReference) throws DicomException {
//long startTime=System.currentTimeMillis();
		ArrayList recordsAsMapsOfStrings = new ArrayList();
		try {
			if (ie != null) {
				String tableName = ie.toString().toUpperCase();
				StringBuffer b = new StringBuffer();
				b.append("SELECT * FROM ");
				b.append(tableName);
				if (ie != rootInformationEntity && localParentReference != null) {
					b.append(" WHERE ");
					b.append(localParentReferenceColumnName);
					//b.append(" LIKE \'");
					b.append(" = \'");
					b.append(localParentReference);
					b.append("\'");
				}
				b.append(";");
				Statement s = databaseConnection.createStatement();
				ResultSet r = s.executeQuery(b.toString());
//System.err.println("Select all values for all records of "+ie.toString()+" with specified parent select only time "+(System.currentTimeMillis()-startTime)+" milliseconds");
				String[] columnNames = getArrayOfColumnNamesForSpecifiedInformationEntity(ie,r);
//System.err.println("Select all values for all records of "+ie.toString()+" with specified parent get column names only time "+(System.currentTimeMillis()-startTime)+" milliseconds");
				int numberOfColumns = columnNames.length;
//long timeInNextResult = 0;
//long timeInMappingColumns = 0;
//long resultSetIteratorStartTime = System.currentTimeMillis();
				while (r.next()) {
//long resultSetIteratorEndTime = System.currentTimeMillis();
//timeInNextResult+=(resultSetIteratorEndTime-resultSetIteratorStartTime);
					TreeMap map = new TreeMap();
//long mappingColumnsStartTime = System.currentTimeMillis();
					for (int i=1; i<=numberOfColumns; ++i) {
						String key = columnNames[i-1];
						String value = r.getString(i);
//System.err.println("findAllAttributeValuesForAllRecordsForThisInformationEntityWithSpecifiedParent: ["+i+"] key = "+key+" value = "+value);
						map.put(key,value);
					}
//long mappingColumnsEndTime = System.currentTimeMillis();
//timeInMappingColumns+=(mappingColumnsEndTime-mappingColumnsStartTime);
					recordsAsMapsOfStrings.add(map);
//resultSetIteratorStartTime = System.currentTimeMillis();
				}
//System.err.println("Select all values for all records of "+ie.toString()+" with specified parent select timeInNextResult "+timeInNextResult+" milliseconds");
//System.err.println("Select all values for all records of "+ie.toString()+" with specified parent select timeInMappingColumns "+timeInMappingColumns+" milliseconds");
				s.close();
			}
		} catch (Exception e) {
                        e.printStackTrace(System.err);
			throw new DicomException("Cannot perform selection: "+e);
		}
//System.err.println("Select all values for all records of "+ie.toString()+" with specified parent time "+(System.currentTimeMillis()-startTime)+" milliseconds");
		return recordsAsMapsOfStrings;
	}

	/**
	 * <p>For all records of an information entity with all parents matching a particular attribute value, get the values of all attributes.</p>
	 *
	 * <p>For example, for the instance entity, this could return the instance local file name for all the series (parents) with a particular frame of reference UID.</p>
	 *
	 * @param	ieWanted		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} of the record level of the attribute to be returned
	 * @param	ieParent		the {@link com.pixelmed.dicom.InformationEntity InformationEntity} of the record level of the parent to be matched
	 * @param	parentMatchingAttribute	the string name of the attribute of the parent whose value is to be matched
	 * @param	parentMatchingValue	the string value of the attribute of the parent to be matched (not including wildcards)
	 * @return				an {@link java.util.ArrayList ArrayList} of records, each value of which is a
	 *					{@link java.util.TreeMap TreeMap} of {@link java.lang.String String} values indexed by {@link java.lang.String String} column names
	 * @exception	DicomException		thrown if the query fails
	 */
	public ArrayList findAllAttributeValuesForAllRecordsForThisInformationEntityWithMatchingParent(
			InformationEntity ieWanted,InformationEntity ieParent,String parentMatchingAttribute,String parentMatchingValue) throws DicomException {
//long startTime=System.currentTimeMillis();
		ArrayList recordsAsMapsOfStrings = new ArrayList();
		try {
			if (ieWanted != null && ieParent != null) {
				String wantedTableName = ieWanted.toString().toUpperCase();
				String parentTableName = ieParent.toString().toUpperCase();
				StringBuffer b = new StringBuffer();
				b.append("SELECT * FROM ");
				b.append(wantedTableName);
				b.append(",");
				b.append(parentTableName);
				b.append(" WHERE ");
				b.append(wantedTableName);
				b.append(".");
				b.append(localParentReferenceColumnName.toUpperCase());
				b.append(" = ");
				b.append(parentTableName);
				b.append(".");
				b.append(localPrimaryKeyColumnName.toUpperCase());
				b.append(" AND ");
				b.append(parentTableName);
				b.append(".");
				b.append(parentMatchingAttribute.toUpperCase());
				//b.append(" LIKE \'");
				b.append(" = \'");
				b.append(parentMatchingValue);
				b.append("\'");
				b.append(";");
				String str = b.toString();
//System.err.println("findSelectedAttributeValueForAllRecordsForThisInformationEntityWithMatchingParent: "+str);
				Statement s = databaseConnection.createStatement();
				ResultSet r = s.executeQuery(str);
				ResultSetMetaData md = r.getMetaData();
				int numberOfColumns = md.getColumnCount();
				while (r.next()) {
					TreeMap map = new TreeMap();
					for (int i=1; i<=numberOfColumns; ++i) {
						String key = md.getColumnName(i);
						String value = r.getString(i);
//System.err.println("findSelectedAttributeValueForAllRecordsForThisInformationEntityWithMatchingParent: ["+i+"] key = "+key+" value = "+value);
						map.put(key,value);
					}
					recordsAsMapsOfStrings.add(map);
				}
				s.close();
			}
		} catch (Exception e) {
                        e.printStackTrace(System.err);
			throw new DicomException("Cannot perform selection: "+e);
		}
//System.err.println("Select all values for all records of "+ieWanted.toString()+" with matching parent time "+(System.currentTimeMillis()-startTime)+" milliseconds");
		return recordsAsMapsOfStrings;
	}

	/**
	 * <p>For an information entity (regardless of a particular instance), find the next information entity lower down in the information model hierarchy in the general case.</p>
	 *
	 * <p>For a patient, this might be a study.</p>
	 * <p>For a series, this might be a concatenation
	 * or an instance, depending on the information model. 
	 *
	 * <p>This method essentially returns the most complex model possible and is used when building the database table schema.</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity} that is the parent, such as a patient,  study, etc.
	 * @return				the {@link com.pixelmed.dicom.InformationEntity InformationEntity} that is the child
	 */
	public abstract InformationEntity getChildTypeForParent(InformationEntity ie);
	
	/**
	 * <p>For a particular instance of an information entity, find the next information entity lower down in the information model hierarchy.</p>
	 *
	 * <p>For a patient, this might be a study. For a series, this might be a concatenation
	 * or an instance, depending on both the information model and the contents of the instance itself. 
	 *
	 * <p>This method may return a simpler view than the more general method, and is used when traversing the database tables.</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity} that is the parent, such as a
	 *					patient,  study, etc.
	 * @param	list			the {@link com.pixelmed.dicom.AttributeList AttributeList} that are the contents the instance
	 * @return				the {@link com.pixelmed.dicom.InformationEntity InformationEntity} that is the child
	 */
	public abstract InformationEntity getChildTypeForParent(InformationEntity ie,AttributeList list);

	/**
	 * <p>For a particular instance of an information entity, find a descriptive name for the entity suitable for rendering.</p>
	 *
	 * <p>For a patient, this might be the name <code>Patient</code>.
	 * For an instance, this will depend on the SOPClassUID, and might be an <code>Image</code>, a <code>waveform</code>, etc.</p>
	 *
	 * @param	ie			the {@link com.pixelmed.dicom.InformationEntity InformationEntity}, such as a
	 *					patient,  study, etc.
	 * @param	returnedAttributes	the attributes from the selected row of the table for this instance of the entity
	 * @return				a human-readable string name of the entity
	 */
	public abstract String getNametoDescribeThisInstanceOfInformationEntity(InformationEntity ie,Map returnedAttributes);
	
	/**
	 * <p>For a particular information entity, find the name of the column in the entity's database table containing the first of three descriptive attributes for the entity.</p>
	 *
	 * <p>For example, for an instance (e.g. an image), this might be the <code>InstanceNumber</code>. 
	 * For a patient, this might be the <code>PatientName</code>.</p>
	 *
	 * @param	ie	the {@link com.pixelmed.dicom.InformationEntity InformationEntity}, such as a
	 *			patient,  study, etc.
	 * @return		the string name of the column, or <code>null</code> if there is no such column
	 */
	public abstract String getDescriptiveColumnName(InformationEntity ie);
	
	/**
	 * <p>For a particular information entity, find the name of the column in the entity's database table containing the second of three descriptive attributes for the entity.</p>
	 *
	 * <p>For example, for an instance (e.g. an image), this might be the <code>InConcatenationNumber</code>. 
	 * Frequently null for other entities.</p>
	 *
	 * @param	ie	the {@link com.pixelmed.dicom.InformationEntity InformationEntity}, such as a
	 *			patient,  study, etc.
	 * @return		the string name of the column, or <code>null</code> if there is no such column
	 */
	public abstract String getOtherDescriptiveColumnName(InformationEntity ie);

	/**
	 * <p>For a particular information entity, find the name of the column in the entity's database table containing the third of three descriptive attributes for the entity.</p>
	 *
	 * <p>For example, for an instance (e.g. an image), this might be the <code>ImageComments</code>. 
	 * For a patient, this might be the <code>PatientID</code>.</p>
	 *
	 * @param	ie	the {@link com.pixelmed.dicom.InformationEntity InformationEntity}, such as a
	 *			patient,  study, etc.
	 * @return		the string name of the column, or <code>null</code> if there is no such column
	 */
	public abstract String getOtherOtherDescriptiveColumnName(InformationEntity ie);
	
	/**
	 * <p>For a particular information entity, find the name of the column in the entity's database table containing the primary key of an instance of the entity.</p>
	 *
	 * @param	ie	the {@link com.pixelmed.dicom.InformationEntity InformationEntity}, such as a
	 *			patient,  study, etc.
	 * @return		the string name of the column	
	 */
	public String getLocalPrimaryKeyColumnName(InformationEntity ie) { return localPrimaryKeyColumnName; }
	
	/**
	 * <p>For a particular information entity, find the name of the column in the entity's database table containing the local file name of a stored object.</p>
	 *
	 * @param	ie	the {@link com.pixelmed.dicom.InformationEntity InformationEntity}, such as a
	 *			instance (e.g. an image)
	 * @return		the string name of the column	
	 */
	public String localFileNameColumnName(InformationEntity ie) { return localFileName; }
	
	/**
	 * <p>For a particular information entity, find the name of the column in the entity's database table containing the reference to an instance of the entity's parent.</p>
	 *
	 * @param	ie	the {@link com.pixelmed.dicom.InformationEntity InformationEntity}, such as a
	 *			patient,  study, etc.
	 * @return		the string name of the column	
	 */
	public String getLocalParentReferenceColumnName(InformationEntity ie) { return localParentReferenceColumnName; }

	/**
	 * <p>Given a DICOM Value Representation, determine the appropriate corresponding SQL type to use.</p>
	 *
	 * <p>For example, the DICOM AE VR should be represented as a CHAR(16), an SS as an INTEGER, and so on.</p>
	 *
	 * @param	vr	the 2 letter DICOM Value Representation as an array of two ASCII bytes.
	 * @return		the string representing the SQL type.
	 */
	public static String getSQLTypeFromDicomValueRepresentation(byte[] vr) {
		String s;
		if (ValueRepresentation.isApplicationEntityVR(vr)) {
			s="CHAR(16)";
		}
		else if (ValueRepresentation.isAgeStringVR(vr)) {
			s="CHAR(4)";
		}
		else if (ValueRepresentation.isCodeStringVR(vr)) {
			s="CHAR(16)";
		}
		else if (ValueRepresentation.isDateVR(vr)) {
			//s="DATE";
			s="CHAR(10)";
		}
		else if (ValueRepresentation.isDateTimeVR(vr)) {
			//s="DATETIME";
			s="CHAR(14)";
		}
		else if (ValueRepresentation.isDecimalStringVR(vr)) {
			//s="REAL";	// this fails if VM > 1, e.g. ImagePositionPatient
			s="VARCHAR(255)";
		}
		else if (ValueRepresentation.isFloatDoubleVR(vr)) {
			s="REAL";	// this will fail if VM > 1, so need to check to be sure only first value inserted
		}
		else if (ValueRepresentation.isFloatSingleVR(vr)) {
			s="REAL";	// this will fail if VM > 1, so need to check to be sure only first value inserted
		}
		else if (ValueRepresentation.isIntegerStringVR(vr)) {
			s="INTEGER";	// this will fail if VM > 1, so need to check to be sure only first value inserted
		}
		else if (ValueRepresentation.isLongStringVR(vr)) {
			s="VARCHAR(255)";
		}
		else if (ValueRepresentation.isLongTextVR(vr)) {
			s="VARCHAR(255)";
		}
		else if (ValueRepresentation.isOtherByteVR(vr)) {
			s=null;
		}
		else if (ValueRepresentation.isOtherWordVR(vr)) {
			s=null;
		}
		else if (ValueRepresentation.isOtherUnspecifiedVR(vr)) {
			s=null;
		}
		else if (ValueRepresentation.isPersonNameVR(vr)) {
			s="VARCHAR(255)";
		}
		else if (ValueRepresentation.isSequenceVR(vr)) {
			s=null;
		}
		else if (ValueRepresentation.isShortStringVR(vr)) {
			s="VARCHAR(255)";
		}
		else if (ValueRepresentation.isSignedLongVR(vr)) {
			s="INTEGER";	// this will fail if VM > 1, so need to check to be sure only first value inserted
		}
		else if (ValueRepresentation.isSignedShortVR(vr)) {
			s="INTEGER";	// this will fail if VM > 1, so need to check to be sure only first value inserted
		}
		else if (ValueRepresentation.isShortTextVR(vr)) {
			s="VARCHAR(255)";
		}
		else if (ValueRepresentation.isTimeVR(vr)) {
			//s="TIME";
			s="CHAR(8)";
		}
		else if (ValueRepresentation.isUniqueIdentifierVR(vr)) {
			s="CHAR(64)";
		}
		else if (ValueRepresentation.isUnsignedLongVR(vr)) {
			s="INTEGER";	// this will fail if VM > 1, so need to check to be sure only first value inserted
		}
		else if (ValueRepresentation.isUnknownVR(vr)) {
			s=null;
		}
		else if (ValueRepresentation.isUnsignedShortVR(vr)) {
			s="INTEGER";	// this will fail if VM > 1, so need to check to be sure only first value inserted
		}
		else if (ValueRepresentation.isUnlimitedTextVR(vr)) {
			s="VARCHAR(255)";
		}
		else {
			s=null;		// unrecognized  ...
		}
		return s;
	}
        
        /**
	 * <p>Make a quoted string value suitable for using in a SQL statement from a DICOM attribute.</p>
	 *
	 * <p>Special characters should be escaped (actually they are just replaced with a hyphen).</p>
	 *
	 * <p>Multiple values are collapsed and separated by the usual DICOM backslash delimiter character (which doesn't bother SQL).</p>
	 *
         * @param	a	the DICOM attribute, which may be null, zero length or multi-valued
	 * @return		the quoted string value of the attribute, or the (unquoted) string NULL if attribute is absent, has no value or is zero length
         * @exception		DicomException
         */
        public static String getQuotedEscapedSingleStringValueOrNull(Attribute a) throws DicomException {
		StringBuffer b = new StringBuffer();
		if (a == null) {
			b.append("NULL");
		}
		else {
			String sqlType = getSQLTypeFromDicomValueRepresentation(a.getVR());
			String v[]=a.getStringValues();
			if (v == null || v.length == 0) {
				b.append("NULL");
			}
			else if (sqlType.equals("INTEGER")) {		// only VM of 1 allowed - use 1st if multiple values; also check range (e.g. some Philips Series Numbers violate IS range)
				long[] iv = a.getLongValues();
				if (iv.length > 0 && (iv[0] < SQL_INTEGER_MINIMUM || iv[0] > SQL_INTEGER_MAXIMUM)) {
//System.err.println("Suppressing too long integer value "+Long.toString(iv[0]));
					b.append("NULL");
				}
				else {
					String s = a.getSingleStringValueOrNull();
					b.append(s == null ? "NULL" : s);
				}
			}
			else if (sqlType.equals("REAL")) {		// only VM of 1 allowed - use 1st if multiple values
				String s = a.getSingleStringValueOrNull();
				b.append(s == null ? "NULL" : s);
			}
			else {
				b.append("\'");
				for (int i=0; i<v.length; ++i) {
					if (i > 0) b.append("\\");
					String s = v[i];
					// need to escape internal single-quote, percent and underscore ... :(
					// for now just replace them with all with a hyphen
					if (s != null) {
						s = s.replace('\'','-');
						s = s.replace('%','-');
						s = s.replace('_','-');
						b.append(s);
					}
				}
				b.append("\'");
			}
		}
                return b.toString();
        }

	/**
	 * <p>Returns a string describing the structure (not the contents) of the database.</p>
	 *
	 * @return	a list of all the attributes for each information entities
	 * 		followed by a description of each table obtained from the
	 *		database metadata, if a connection can be established, or the string
	 * 		value of the exception if it cannot.
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();

		// dump listsOfAttributesByInformationEntity

		buffer.append(listsOfAttributesByInformationEntity);
		buffer.append("\n");

		// dump table descriptions from database metadata

		try {
			DatabaseMetaData meta = databaseConnection.getMetaData();
			ResultSet columns = meta.getColumns(null, null, null, null);

			while (columns.next()) {
				buffer.append("catalog = "); buffer.append(columns.getString(1)); buffer.append("; ");
				buffer.append("schema  = "); buffer.append(columns.getString(2)); buffer.append("; ");
				buffer.append("table   = "); buffer.append(columns.getString(3)); buffer.append("; ");
				buffer.append("column  = "); buffer.append(columns.getString(4)); buffer.append("; ");
				buffer.append("type    = "); buffer.append(columns.getString(6)); buffer.append("; ");
				buffer.append("size    = "); buffer.append(columns.getInt(7)); buffer.append("; ");
				buffer.append("\n");
			}

			columns.close();

		}
		catch (Exception e) {
			//throw new DicomException("Cannot connect to database: "+e);
			buffer.append(e);
		}
		return buffer.toString();
	}
}
