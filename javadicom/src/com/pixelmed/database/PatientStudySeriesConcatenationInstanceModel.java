/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.database;

import com.pixelmed.dicom.*;

import java.sql.*;
import java.util.Iterator;
import java.util.Map;

// the following are only for main test to use DatabaseTreeBrowser ...

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

/**
 * <p>The {@link com.pixelmed.database.PatientStudySeriesConcatenationInstanceModel PatientStudySeriesConcatenationInstanceModel} class
 * supports a simple DICOM Patient/Study/Series/Concatenation/Instance model.</p>
 *
 * <p>Attributes of other DICOM entities than Patient, Study, Series, Concatenation and Instance are included at the appropriate lower level entity.</p>
 *
 * @see com.pixelmed.database.DicomDictionaryForPatientStudySeriesConcatenationInstanceModel
 *
 * @author	dclunie
 */
public class PatientStudySeriesConcatenationInstanceModel extends DicomDatabaseInformationModel {

	/***/
	private static final String identString = "@(#) $Header$";

	/**
	 * @param	databaseName
	 * @exception	DicomException
	 */
	public PatientStudySeriesConcatenationInstanceModel(String databaseName) throws DicomException {
		super(databaseName,InformationEntity.PATIENT,new DicomDictionaryForPatientStudySeriesConcatenationInstanceModel());
	}

	/**
	 * @param	ie	the information entity
	 * @return		true if the information entity is in the model
	 */
	protected boolean isInformationEntityInModel(InformationEntity ie) {
		return ie == InformationEntity.PATIENT
		    || ie == InformationEntity.STUDY
		//  || ie == InformationEntity.PROCEDURESTEP
		    || ie == InformationEntity.SERIES
		    || ie == InformationEntity.CONCATENATION
		    || ie == InformationEntity.INSTANCE
		//  || ie == InformationEntity.FRAME
		    ;
	}

	/**
	 * @param	ie			the parent information entity
	 * @param	concatenationUID	the ConcatenationUID, if present, else null, as a flag to use concatenations in the model or not
	 * @return				the child information entity
	 */
	private InformationEntity getChildTypeForParent(InformationEntity ie,String concatenationUID) {
		if      (ie == InformationEntity.PATIENT)       return InformationEntity.STUDY;
		else if (ie == InformationEntity.STUDY)         return InformationEntity.SERIES;
		//else if (ie == InformationEntity.STUDY)         return InformationEntity.PROCEDURESTEP;
		//else if (ie == InformationEntity.PROCEDURESTEP) return InformationEntity.SERIES;
		//else if (ie == InformationEntity.SERIES)        return InformationEntity.INSTANCE;
		else if (ie == InformationEntity.SERIES)        return concatenationUID == null ? InformationEntity.INSTANCE : InformationEntity.CONCATENATION;
		else if (ie == InformationEntity.CONCATENATION) return InformationEntity.INSTANCE;
		//else if (ie == InformationEntity.INSTANCE)      return InformationEntity.FRAME;
		//else if (ie == InformationEntity.FRAME)         return null;
		else return null;
	}

	/**
	 * @param	ie	the parent information entity
	 * @return		the child information entity
	 */
	public InformationEntity getChildTypeForParent(InformationEntity ie) {
		return getChildTypeForParent(ie,"");		// not null ... this method is called e.g. when creating tables, so assume concatenation
	}

	/**
	 * @param	ie	the parent information entity
	 * @param	list	an AttributeList, in which ConcatenationUID may or may not be present,as a flag to use concatenations in the model or not
	 * @return		the child information entity
	 */
	public InformationEntity getChildTypeForParent(InformationEntity ie,AttributeList list) {
		String concatenationUID = Attribute.getSingleStringValueOrNull(list,TagFromName.ConcatenationUID);
		return getChildTypeForParent(ie,concatenationUID);
	}

	/**
	 * @param	ie	the information entity
	 * @return		the name of a column in the table that describes the instance of the information entity
	 */
	public String getDescriptiveColumnName(InformationEntity ie) {
		if      (ie == InformationEntity.PATIENT)       return "PatientName";
		else if (ie == InformationEntity.STUDY)         return "StudyDate";			// to allow tree sort by study date first ([bugs.mrmf] (000111) Studies in browser not sorted by date but ID, and don't display date)
		else if (ie == InformationEntity.PROCEDURESTEP) return null;
		else if (ie == InformationEntity.SERIES)        return "SeriesNumber";
		else if (ie == InformationEntity.CONCATENATION) return "InstanceNumber";
		else if (ie == InformationEntity.INSTANCE)      return "InstanceNumber";
		else if (ie == InformationEntity.FRAME)         return null;
		else return null;
	}

	/**
	 * @param	ie	the information entity
	 * @return		the name of another column in the table that describes the instance of the information entity
	 */
	public String getOtherDescriptiveColumnName(InformationEntity ie) {
		if      (ie == InformationEntity.PATIENT)       return null;
		else if (ie == InformationEntity.STUDY)         return "StudyID";			// [bugs.mrmf] (000111) Studies in browser not sorted by date but ID, and don't display date
		else if (ie == InformationEntity.PROCEDURESTEP) return null;
		else if (ie == InformationEntity.SERIES)        return null;
		else if (ie == InformationEntity.CONCATENATION) return null;
		else if (ie == InformationEntity.INSTANCE)      return "InConcatenationNumber";
		else if (ie == InformationEntity.FRAME)         return null;
		else return null;
	}

	/**
	 * @param	ie	the information entity
	 * @return		the name of yet another column in the table that describes the instance of the information entity
	 */
	public String getOtherOtherDescriptiveColumnName(InformationEntity ie) {
		if      (ie == InformationEntity.PATIENT)       return "PatientID";
		else if (ie == InformationEntity.STUDY)         return "StudyDescription";
		else if (ie == InformationEntity.PROCEDURESTEP) return null;
		else if (ie == InformationEntity.SERIES)        return "SeriesDescription";
		else if (ie == InformationEntity.CONCATENATION) return null;
		else if (ie == InformationEntity.INSTANCE)      return "ImageComments";
		else if (ie == InformationEntity.FRAME)         return null;
		else return null;
	}

	/**
	 * @param	b
	 * @param	ie
	 */
	protected void extendCreateStatementStringWithAnyExtraAttributes(StringBuffer b,InformationEntity ie) {
		if (ie == InformationEntity.INSTANCE) {		// since dictionary now says InstanceNumber is in Concatenation IE
								// but we want the UID in both IE's so tree browser can show it
								// at either concatenation or image level depending on whether
								// Concatenation is present (as flagged by Concatenation UID)
			String columnName = "InstanceNumber";
			String columnType = getSQLTypeFromDicomValueRepresentation(ValueRepresentation.IS);
			b.append(", ");
			b.append(columnName);
			b.append(" ");
			b.append(columnType);
		}
	}

	/**
	 * @param	b
	 * @param	list
	 * @param	ie
	 * @exception	DicomException
	 */
	protected void extendStatementStringWithMatchingAttributesForSelectedInformationEntity(StringBuffer b,AttributeList list,InformationEntity ie) throws DicomException {

		// two possibilities ...
		// 1. iterate through whole list of attributes and insist on match for all present for that IE
		// 2. be more selective ... consider match only on "unique key(s)" for a particular level and ignore others
		//
		// adopt the latter approach ...

		// also need to escape wildcards and so on, but ignore for now ...

		if      (ie == InformationEntity.PATIENT) {
			// no AND since would be no parent reference preceding
			//b.append("PATIENT.PatientID LIKE ");
			b.append("PATIENT.PatientID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.PatientID)));
		}
		else if (ie == InformationEntity.STUDY) {
			b.append(" AND ");
			//b.append("STUDY.StudyInstanceUID LIKE ");
			b.append("STUDY.StudyInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.StudyInstanceUID)));
		}
		else if (ie == InformationEntity.PROCEDURESTEP) {
		}
		else if (ie == InformationEntity.SERIES) {
			b.append(" AND ");
			//b.append("SERIES.SeriesInstanceUID LIKE ");
			b.append("SERIES.SeriesInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SeriesInstanceUID)));
		}
		else if (ie == InformationEntity.CONCATENATION) {
			Attribute concatenationUID = list.get(TagFromName.ConcatenationUID);
			Attribute instanceNumber = list.get(TagFromName.InstanceNumber);
			if (concatenationUID != null) {
				b.append(" AND ");
				//b.append("CONCATENATION.ConcatenationUID LIKE ");
				b.append("CONCATENATION.ConcatenationUID = ");
				b.append(getQuotedEscapedSingleStringValueOrNull(concatenationUID));
			}
			else {
				b.append(" AND ");
				//b.append("CONCATENATION.InstanceNumber LIKE ");
				b.append("CONCATENATION.InstanceNumber = ");
				b.append(getQuotedEscapedSingleStringValueOrNull(instanceNumber));
			}
		}
		else if (ie == InformationEntity.INSTANCE) {
			b.append(" AND ");
			//b.append("INSTANCE.SOPInstanceUID LIKE ");
			b.append("INSTANCE.SOPInstanceUID = ");
			b.append(getQuotedEscapedSingleStringValueOrNull(list.get(TagFromName.SOPInstanceUID)));
		}
		else if (ie == InformationEntity.FRAME) {
		}
	}

	/**
	 * <p>For unit test
	 * purposes.</p>
	 *
	 * <p>Reads the DICOM files listed on the command line, loads them into the model and pops up a browser
	 * for viewing the tree hierarchy of the model and the values of each instance of an entity.</p>
	 *
	 * @param	arg	a list of DICOM file names
	 */
	public static void main(String arg[]) {
		try {
			final DatabaseInformationModel d = new PatientStudySeriesConcatenationInstanceModel("test");

			for (int j=0; j<arg.length; ++j) {
				String fileName = arg[j];
System.err.println("reading "+fileName);
				DicomInputStream dfi = new DicomInputStream(new BufferedInputStream(new FileInputStream(fileName)));
				AttributeList list = new AttributeList();
				list.read(dfi);
				dfi.close();
				//d.extendTablesAsNecessary(list);		// doesn't work with Hypersonic ... ALTER command not supported
System.err.println("inserting");
				d.insertObject(list,fileName);
			}
//System.err.print(d);
			final JFrame frame = new JFrame();
			frame.setSize(400,800);
			frame.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					frame.dispose();
					d.close();
					System.exit(0);
				}
			});
System.err.println("building tree");
			DatabaseTreeBrowser tree = new DatabaseTreeBrowser(d,frame);
System.err.println("display tree");
			frame.show(); 
		} catch (Exception e) {
			System.err.println(e);
                        e.printStackTrace(System.err);
			System.exit(0);
		}
	}
}

