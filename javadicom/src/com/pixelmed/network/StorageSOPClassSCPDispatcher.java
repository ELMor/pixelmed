/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

import com.pixelmed.dicom.*;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;

/**
 * <p>This class waits for incoming connections and association requests for
 * the SCP role of SOP Classes of the Storage Service Class, and the Verification SOP Class.</p>
 *
 * <p>The class has a constructor and a <code>run()</code> method. The
 * constructor is passed a socket on which to listen for transport
 * connection open indications. The <code>run()</code> method waits
 * for transport connection open indications, then instantiates
 * {@link com.pixelmed.network.StorageSOPClassSCP StorageSOPClassSCP}
 * to accept an association and wait for storage or verification commands, storing
 * data sets in Part 10 files in the specified folder.</p>
 *
 * <p>An instance of {@link com.pixelmed.network.ReceivedObjectHandler ReceivedObjectHandler}
 * can be supplied in the constructor to process the received data set stored in the file
 * when it has been completely received.</p>
 *
 * <p>For example:</p>
 * <pre>
try {
  new Thread(new StorageSOPClassSCPDispatcher("104","STORESCP","/tmp",new OurReceivedObjectHandler(),0)).start();
}
catch (IOException e) {
  e.printStackTrace(System.err);
}
 * </pre>
 *
 * <p>Debugging messages with a varying degree of verbosity can be activated.</p>
 *
 * <p>The main method is also useful in its own right as a command-line Storage
 * SCP utility, which will store incoming files in a specified directory.</p>
 *
 * <p>For example:</p>
 * <pre>
% java -cp ./pixelmed.jar com.pixelmed.network.StorageSOPClassSCPDispatcher "104" "STORESCP" "/tmp" 0
 * </pre>
 *
 * @see com.pixelmed.network.StorageSOPClassSCP
 * @see com.pixelmed.network.ReceivedObjectHandler
 *
 * @author	dclunie
 */
public class StorageSOPClassSCPDispatcher implements Runnable {
	/***/
	private static final String identString = "@(#) $Header$";
	
	/***/
	private class OurReceivedObjectHandler extends ReceivedObjectHandler {
		/**
		 * @param	fileName
		 * @param	transferSyntax		the transfer syntax in which the data set was received and is stored
		 * @param	callingAETitle		the AE title of the caller who sent the data set
		 * @exception	IOException
		 * @exception	DicomException
		 * @exception	DicomNetworkException
		 */
		public void sendReceivedObjectIndication(String fileName,String transferSyntax,String callingAETitle)
				throws DicomNetworkException, DicomException, IOException {
System.err.println("StorageSOPClassSCPDispatcher.OurReceivedObjectHandler.sendReceivedObjectIndication() fileName: "+fileName+" from "+callingAETitle+" in "+transferSyntax);
		}
	}
	
	/***/
	private String port;
	/***/
	private String calledAETitle;
	/***/
	private int ourMaximumLengthReceived;
	/***/
	private int socketReceiveBufferSize;
	/***/
	private int socketSendBufferSize;
	/***/
	private File savedImagesFolder;
	/***/
	private ReceivedObjectHandler receivedObjectHandler;
	/***/
	private int debugLevel;

	/**
	 * <p>Construct an instance of dispatcher that will wait for transport
	 * connection open indications, and handle associations and commands.</p>
	 *
	 * @param	port				the port on which to listen for connections
	 * @param	calledAETitle			our AE Title
	 * @param	ourMaximumLengthReceived	the maximum PDU length that we will offer to receive
	 * @param	socketReceiveBufferSize		the TCP socket receive buffer size to set (if possible), 0 means leave at the default
	 * @param	socketSendBufferSize		the TCP socket send buffer size to set (if possible), 0 means leave at the default
	 * @param	receivedObjectHandler		the handler to call after each data set has been received and stored
	 * @param	savedImagesFolder		the folder in which to store received data sets (may be null, to ignore received data for testing)
	 * @param	debugLevel			zero for no debugging messages, higher values more verbose messages
	 * @exception	IOException
	 */
	public StorageSOPClassSCPDispatcher(String port,String calledAETitle,
			int ourMaximumLengthReceived,int socketReceiveBufferSize,int socketSendBufferSize,
			File savedImagesFolder,ReceivedObjectHandler receivedObjectHandler,int debugLevel) throws IOException {
		this.port=port;
		this.calledAETitle=calledAETitle;
		this.ourMaximumLengthReceived=ourMaximumLengthReceived;
		this.socketReceiveBufferSize=socketReceiveBufferSize;
		this.socketSendBufferSize=socketSendBufferSize;
		this.savedImagesFolder=savedImagesFolder;
		this.receivedObjectHandler=receivedObjectHandler;
		this.debugLevel=debugLevel;
	}

	/**
	 * <p>Construct an instance of dispatcher that will wait for transport
	 * connection open indications, and handle associations and commands.</p>
	 *
	 * @param	port			the port on which to listen for connections
	 * @param	calledAETitle		our AE Title
	 * @param	receivedObjectHandler	the handler to call after each data set has been received and stored
	 * @param	savedImagesFolder	the folder in which to store received data sets (may be null, to ignore received data for testing)
	 * @param	debugLevel		zero for no debugging messages, higher values more verbose messages
	 * @exception	IOException
	 */
	public StorageSOPClassSCPDispatcher(String port,String calledAETitle,File savedImagesFolder,ReceivedObjectHandler receivedObjectHandler,int debugLevel) throws IOException {
		this.port=port;
		this.calledAETitle=calledAETitle;
		this.ourMaximumLengthReceived=AssociationFactory.getDefaultMaximumLengthReceived();
		this.socketReceiveBufferSize=AssociationFactory.getDefaultReceiveBufferSize();
		this.socketSendBufferSize=AssociationFactory.getDefaultSendBufferSize();
		this.savedImagesFolder=savedImagesFolder;
		this.receivedObjectHandler=receivedObjectHandler;
		this.debugLevel=debugLevel;
	}

	/**
	 * <p>Construct an instance of dispatcher that will wait for transport
	 * connection open indications, and handle associations and commands.</p>
	 *
	 * <p>No further action is taken after each data set is received other than to store it.</p>
	 *
	 * @param	port				the port on which to listen for connections
	 * @param	calledAETitle			our AE Title
	 * @param	ourMaximumLengthReceived	the maximum PDU length that we will offer to receive
	 * @param	socketReceiveBufferSize		the TCP socket receive buffer size to set (if possible), 0 means leave at the default
	 * @param	socketSendBufferSize		the TCP socket send buffer size to set (if possible), 0 means leave at the default
	 * @param	savedImagesFolder		the folder in which to store received data sets (may be null, to ignore received data for testing)
	 * @param	debugLevel			zero for no debugging messages, higher values more verbose messages
	 * @exception	IOException
	 */
	public StorageSOPClassSCPDispatcher(String port,String calledAETitle,
			int ourMaximumLengthReceived,int socketReceiveBufferSize,int socketSendBufferSize,
			File savedImagesFolder,int debugLevel) throws IOException {
		this.port=port;
		this.calledAETitle=calledAETitle;
		this.ourMaximumLengthReceived=ourMaximumLengthReceived;
		this.socketReceiveBufferSize=socketReceiveBufferSize;
		this.socketSendBufferSize=socketSendBufferSize;
		this.savedImagesFolder=savedImagesFolder;
		receivedObjectHandler=new OurReceivedObjectHandler();
		this.debugLevel=debugLevel;
	}

	/**
	 * <p>Construct an instance of dispatcher that will wait for transport
	 * connection open indications, and handle associations and commands.</p>
	 *
	 * <p>No further action is taken after each data set is received other than to store it.</p>
	 *
	 * @param	port			the port on which to listen for connections
	 * @param	calledAETitle		our AE Title
	 * @param	savedImagesFolder	the folder in which to store received data sets (may be null, to ignore received data for testing)
	 * @param	debugLevel		zero for no debugging messages, higher values more verbose messages
	 * @exception	IOException
	 */
	public StorageSOPClassSCPDispatcher(String port,String calledAETitle,File savedImagesFolder,int debugLevel) throws IOException {
		this.port=port;
		this.calledAETitle=calledAETitle;
		this.ourMaximumLengthReceived=AssociationFactory.getDefaultMaximumLengthReceived();
		this.socketReceiveBufferSize=AssociationFactory.getDefaultReceiveBufferSize();
		this.socketSendBufferSize=AssociationFactory.getDefaultSendBufferSize();
		this.savedImagesFolder=savedImagesFolder;
		receivedObjectHandler=new OurReceivedObjectHandler();
		this.debugLevel=debugLevel;
	}

	/**
	 * <p>Waits for a transport connection indications, then spawns
	 * new threads to act as association acceptors, which then wait for storage or
	 * verification commands, storing data sets in Part 10 files in the specified folder, until the associations
	 * are released or the transport connections are closed.</p>
	 */
	public void run() {
//System.err.println("StorageSOPClassSCPDispatcher:run()");
		try {
			ServerSocket serverSocket = new ServerSocket(Integer.parseInt(port));
			while (true) {
				Socket socket = serverSocket.accept();
//System.err.println("StorageSOPClassSCPDispatcher:accept() returned");
				//setSocketOptions(socket,ourMaximumLengthReceived,socketReceiveBufferSize,socketSendBufferSize,debugLevel);
				try {
					new Thread(new StorageSOPClassSCP(socket,calledAETitle,
						ourMaximumLengthReceived,socketReceiveBufferSize,socketSendBufferSize,savedImagesFolder,receivedObjectHandler,debugLevel)).start();
				}
				catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * <p>For testing.</p>
	 *
	 * <p>Wait for connections, accept associations and store received files in the specified folder.</p>
	 *
	 * @param	arg	array of four or seven strings - our port, our AE Title,
	 *			optionally the max PDU size, socket receive and send buffer sizes,
	 *			the folder in which to stored received files (zero length if want to ignore received data), and the debugging level
	 */
	public static void main(String arg[]) {
		try {
			StorageSOPClassSCPDispatcher dispatcher = null;
			File savedImagesFolder = null;
			if (arg.length == 4) {
				String savedImagesFolderName = arg[2];
				if (savedImagesFolderName != null && savedImagesFolderName.length() > 0) {
					savedImagesFolder = new File(savedImagesFolderName);
				}
				dispatcher = new StorageSOPClassSCPDispatcher(arg[0],arg[1],savedImagesFolder,Integer.parseInt(arg[3])/*debugLevel*/);
			}
			else if (arg.length == 7) {
				String savedImagesFolderName = arg[5];
				if (savedImagesFolderName != null && savedImagesFolderName.length() > 0) {
					savedImagesFolder = new File(savedImagesFolderName);
				}
				dispatcher = new StorageSOPClassSCPDispatcher(arg[0],arg[1],
					Integer.parseInt(arg[2]),Integer.parseInt(arg[3]),Integer.parseInt(arg[4]),
					savedImagesFolder,Integer.parseInt(arg[6])/*debugLevel*/);
			}
			new Thread(dispatcher).start();
		}
		catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(0);
		}
	}
}