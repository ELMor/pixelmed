/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.network;

/**
 * @author	dclunie
 */
public class AReleaseException extends Exception {

	private static final String identString = "@(#) $Header$";

	/**
	 * @param	msg
	 */
	public AReleaseException(String msg) {
		super(msg);
	}
}



