package com.pixelmed.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

/**
 * <p>Various static methods helpful for handling files.</p>
 *
 * @author	dclunie
 */
public class FileUtilities {
	private static final String identString = "@(#) $Header$";

	private FileUtilities() {}

	/**
	 * <p>Recursively traverse the specified directory and its sub-directory and
	 * produce a list of all the files contained therein, in no particular order.</p>
	 *
	 * <p>If the path is a file, just return that.</p>
	 *
	 * <p>Any security (permission) exceptions are caught and logged to stderr
	 * and not propagated.</p>
	 *
	 * @param	initialPath	The abstract pathname of the directory to begin searching
	 * @return			An ArrayList of abstract pathnames denoting the files found.
	 *				The ArrayList will be empty if the path is empty or does not exist
	 *				or if an error occurs.
	 */
	static public final ArrayList listFilesRecursively(File initialPath) {
//System.err.println("FileUtilities.listFilesRecursively(): "+initialPath);
		ArrayList filesFound = new ArrayList();
		if (initialPath != null && initialPath.exists()) {
			if (initialPath.isFile()) {
				filesFound.add(initialPath);
			}
			else if (initialPath.isDirectory()) {
				try {
					File[] filesAndDirectories = initialPath.listFiles((FilenameFilter)null);	// null FilenameFilter means all names
					if (filesAndDirectories != null && filesAndDirectories.length > 0) {
						for (int i=0; i<filesAndDirectories.length; ++i) {
							if (filesAndDirectories[i].isDirectory()) {
								ArrayList moreFiles = listFilesRecursively(filesAndDirectories[i]);
								if (moreFiles != null && !moreFiles.isEmpty()) {
									filesFound.addAll(moreFiles);
								}
							}
							else if (filesAndDirectories[i].isFile()) {			// what else could it be ... just being paranoid
//System.err.println("FileUtilities.listFilesRecursively(): found "+filesAndDirectories[i]);
								filesFound.add(filesAndDirectories[i]);
							}
						}
					}
				}
				catch (SecurityException e) {
					e.printStackTrace(System.err);
				}
			}
			// else what else could it be
		}
		return filesFound;
	}

}


