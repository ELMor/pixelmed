/* Copyright (c) 2001-2003, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.query;

import java.util.*;
import javax.swing.tree.*;

import com.pixelmed.dicom.*;
import com.pixelmed.utils.StringUtilities;

/**
 * <p>Instances of the {@link com.pixelmed.query.QueryTreeRecord QueryTreeRecord} class represent
 * nodes in a tree of the {@link com.pixelmed.query.QueryTreeModel QueryTreeModel} class, which in
 * turn is used by the {@link com.pixelmed.query.QueryTreeBrowser QueryTreeBrowser} class.</p>
 *
 * <p>This class is publically visible primarily so that selection change listeners can be
 * constructed for {@link com.pixelmed.query.QueryTreeBrowser QueryTreeBrowser}, since
 * the user's selection is returned as a path of {@link com.pixelmed.query.QueryTreeRecord QueryTreeRecord}
 * instances, which need to be cast accordingly.</p>
 *
 * @author	dclunie
 */
public class QueryTreeRecord implements Comparable, TreeNode {

	private static final String identString = "@(#) $Header$";

	QueryTreeRecord parent;
	List children;
	InformationEntity ie;
	AttributeList parentUniqueKeys;
	Attribute uniqueKey;
	AttributeList allAttributesReturnedInIdentifier;
	String value;

	/**
	 * <p>Dump the string value of the node.</p>
	 *
	 * @return	the string value of this node
	 */
	public String toString() {
		return value == null ? "" : value;
	}
	
	// Methods to implement Comparable (allows parent to sort)

	/**
	 * <p>Compare nodes based on the lexicographic order of their string values.</p>
	 *
	 * <p>Note that the comparison is more complex than a simple lexicographic comparison
	 * of strings (as described in the definition of {@link java.lang.String#compareTo(String) java.lang.String.compareTo(String)}
	 * but rather accounts for embedded non-zero padded integers. See {@link com.pixelmed.utils.StringUtilities#compareStringsWithEmbeddedNonZeroPaddedIntegers(String,String) com.pixelmed.utils.compareStringsWithEmbeddedNonZeroPaddedIntegers(String,String)}
	 * </p>
	 * @param	o	the {@link com.pixelmed.query.QueryTreeRecord QueryTreeRecord}
	 *			to compare this {@link com.pixelmed.query.QueryTreeRecord QueryTreeRecord} against
	 * @return		the value 0 if the argument is equal to this object; a value less than 0 if this object
	 *			is lexicographically less than the argument; and a value greater than 0 if this object
	 *			is lexicographically greater than the argument
	 */
	public int compareTo(Object o) {
		String ovalue = ((QueryTreeRecord)o).value;
		//return value.compareTo(ovalue);
		return StringUtilities.compareStringsWithEmbeddedNonZeroPaddedIntegers(value,ovalue);
	}

	/**
	 * @param	o
	 */
	public boolean equals(Object o) {
		return compareTo(o) == 0;
	}

	// Methods to implement TreeNode ...

	/**
	 * <p>Returns the parent node of this node.</p>
	 *
	 * @return	the parent node, or null if the root
	 */
	public TreeNode getParent() {
		return parent;
	}

	/**
	 * <p>Returns the child at the specified index.</p>
	 *
	 * @param	index	the index of the child to be returned, numbered from 0
	 * @return		the child <code>TreeNode</code> at the specified index
	 */
	public TreeNode getChildAt(int index) {
		return (TreeNode)(children.get(index));
	}

	/**
	 * <p>Returns the index of the specified child from amongst this node's children, if present.</p>
	 *
	 * @param	child	the child to search for amongst this node's children
	 * @return		the index of the child, or -1 if not present
	 */
	public int getIndex(TreeNode child) {
//System.err.println("getIndexOfChild: looking for "+child);
		int n=children.size();
		for (int i=0; i<n; ++i) {
			if (children.get(i).equals(child)) {	// expensive comparison ? :(
//System.err.println("getIndexOfChild: found "+child);
				return i;
			}
		}
		return -1;
	}

	/**
	 * <p> Always returns true, since children may always be added.</p>
	 *
	 * @return	always true
	 */
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * <p> Returns true if the receiver is a leaf (has no children).</p>
	 *
	 * @return	true if the receiver is a leaf
	 */
	public boolean isLeaf() {
		return getChildCount() == 0;
	}

	/**
	 * <p>Return the number of children that this node contains.</p>
	 *
	 * @return	the number of children, 0 if none
	 */
	public int getChildCount() {
		return children == null ? 0 : children.size();
	}

	/**
	 * <p>Returns the children of this node as an {@link java.util.Enumeration Enumeration}.</p>
	 *
	 * @return	the children of this node
	 */
	public Enumeration children() {
		return children == null ? null : new Vector(children).elements();
	}

	// Methods specific to this kind of node ...

	/**
	 * <p>Make a new node in a
	 * tree.</p>
	 *
	 * @param	parent					the parent of this node
	 * @param	value					a string value which is used primarily to sort siblings into lexicographic order
	 * @param	ie					the entity in the DICOM information model that the constructed node is an instance of
	 * @param	parentUniqueKeys			a list of DICOM attributes, one for each unique key of each parent of this level
	 * @param	uniqueKey				the DICOM attribute which is the unique key at the level of this record
	 * @param	allAttributesReturnedInIdentifier	a list of all the DICOM attributes from the query response for this level of a query
	 */
	public QueryTreeRecord(QueryTreeRecord parent,String value,InformationEntity ie,AttributeList parentUniqueKeys,Attribute uniqueKey,AttributeList allAttributesReturnedInIdentifier) {
		this.parent=parent;
		this.value=value;
		this.ie=ie;
		this.parentUniqueKeys=parentUniqueKeys;
		this.uniqueKey=uniqueKey;
		this.allAttributesReturnedInIdentifier=allAttributesReturnedInIdentifier;
	}

	/**
	 * <p>Add a child to this nodes sorted collection of children.</p>
	 *
	 * @param	child	the child node to be added
	 */
	public void addChild(QueryTreeRecord child) {
		if (children == null) children=new ArrayList();
		children.add(child);
		Collections.sort(children);
	}

	/**
	 * <p>Add a sibling to this node,
	 * that is add a child to this
	 * node's parent's sorted collection of children.</p>
	 *
	 * @param	sibling		the sibling node to be added
	 * @exception	DicomException	thrown if this node has no parent
	 */
	public void addSibling(QueryTreeRecord sibling) throws DicomException {
		if (parent == null) {
			throw new DicomException("Internal error - root node with sibling");
		}
		else {
			parent.addChild(sibling);
		}
	}

	/**
	 * <p>Get the string value of the node which is used for sorting and human-readable rendering.</p>
	 *
	 * @return	the string value of this node
	 */
	public String getValue() { return value; }
	
	/**
	 * <p>Get the information entity that this node represents.</p>
	 *
	 * @return	information entity that this node represents
	 */
	public InformationEntity getInformationEntity() { return ie; }
	
	/**
	 * <p>Get the list of DICOM attributes, one for each unique key of each parent of this level.</p>
	 *
	 * @return	the list of parents' unique keys
	 */
	public AttributeList getParentUniqueKeys() { return parentUniqueKeys; }
	
	/**
	 * <p>Get the DICOM attribute that is the unique key at the level of this record.</p>
	 *
	 * @return	the unique key
	 */
	public Attribute getUniqueKey() { return uniqueKey; }
	
	/**
	 * <p>Get the list of all the DICOM attributes from the query response for this level of the query.</p>
	 *
	 * @return	the list of all response attributes for this level
	 */
	public AttributeList getAllAttributesReturnedInIdentifier() { return allAttributesReturnedInIdentifier; }
}



