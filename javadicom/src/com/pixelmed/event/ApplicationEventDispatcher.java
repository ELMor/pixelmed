/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.event;

import java.util.Vector;
import java.util.Iterator;

/**
 * @author      dclunie
 */
public class ApplicationEventDispatcher implements EventDispatcher {

	/***/
	static final String identString = "@(#) $Header$";

	public static ApplicationEventDispatcher applicationEventDispatcher;

	/***/
	private Vector listeners = new Vector();		// Vectors are synchronized

	public ApplicationEventDispatcher() throws Exception {
		if (applicationEventDispatcher != null) {
			throw new Exception("Cannot call ApplicationEventDispatcher constructor twice");
		}
		applicationEventDispatcher=this;
	}
	
	/**
	 * @param	l
	 */
	public void addListener(Listener l) {
//System.err.println("ApplicationEventDispatcher.addListener(): listener="+l);
		synchronized (listeners) {
//System.err.println("ApplicationEventDispatcher.addListener(): lock on listeners acquired");
			listeners.addElement(l);
		}
	}

	/**
	 * @param	l
	 */
	public void removeListener(Listener l) {
//System.err.println("ApplicationEventDispatcher.removeListener(): listener="+l);
		synchronized (listeners) {
//System.err.println("ApplicationEventDispatcher.removeListener(): lock on listeners acquired");
			listeners.removeElement(l);
		}
	}

	/**
	 * @param	c
	 */
	//public void removeAllListenersWhichAreOfClass(Class c) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersWhichAreOfClass():");
	//	synchronized (listeners) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersWhichAreOfClass(): lock on listeners acquired");
	//		Iterator i=listeners.iterator();
	//		while (i.hasNext()) {
	//			Listener listener = (Listener)(i.next());
//System.err.println("ApplicationEventDispatcher.removeAllListenersWhichAreOfClass(): looping on listener="+listener);
	//			if (c.isInstance(listener)) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersWhichAreOfClass(): removing listener="+listener);
	//				i.remove();
	//			}
	//		}
	//	}
	//}

	/**
	 * @param	c
	 */
	//public void removeAllListenersForEventsOfClass(Class c) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventsOfClass():");
	//	synchronized (listeners) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventsOfClass(): lock on listeners acquired");
	//		Iterator i=listeners.iterator();
	//		while (i.hasNext()) {
	//			Listener listener = (Listener)(i.next());
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventsOfClass(): looping on listener="+listener);
	//			if (listener.getClassOfEventHandled() == c) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventsOfClass(): removing listener="+listener);
	//				i.remove();
	//			}
	//		}
	//	}
	//}

	/**
	 * @param	context		does nothing if null (i.e., does not remove listeners with null context)
	 */
	public void removeAllListenersForEventContext(EventContext context) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventContext(): context="+context);
		if (context != null) {
			synchronized (listeners) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventContext(): lock on listeners acquired");
				Iterator i=listeners.iterator();
				while (i.hasNext()) {
					Listener listener = (Listener)(i.next());
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventContext(): looping on listener="+listener);
					if (listener.getEventContext() == context) {
//System.err.println("ApplicationEventDispatcher.removeAllListenersForEventContext(): removing listener="+listener);
						i.remove();
					}
				}
			}
		}
	}

	/**
	 * @param	event
	 */
	public synchronized void processEvent(Event event) {
//System.err.println("ApplicationEventDispatcher.processEvent(): event="+event);
		Class eventClass = event.getClass();
		EventContext eventContext = event.getEventContext();
		synchronized (listeners) {
//System.err.println("ApplicationEventDispatcher.processEvent(): lock on listeners acquired");
//System.err.println("ApplicationEventDispatcher.processEvent(): listeners size="+listeners.size());
			Iterator i=listeners.iterator();
			while (i.hasNext()) {
				Listener listener = (Listener)(i.next());
//System.err.println("ApplicationEventDispatcher.processEvent(): looping on listener="+listener);
				EventContext listenerEventContext = listener.getEventContext();
				if (listener.getClassOfEventHandled() == eventClass	// this is inefficient; should use something hashed by event.getClass() :(
				 && (eventContext == null				// events without context match listeners of any context
				  || listenerEventContext == null			// listeners without context match events of any context
				  || listenerEventContext == eventContext)) {
//System.err.println("ApplicationEventDispatcher.processEvent(): matched listener="+listener);
					listener.changed(event);
				}
			}
		}
	}
}
