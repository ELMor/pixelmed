/* Copyright (c) 2001-2004, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package com.pixelmed.event;

/**
 * @author	dclunie
 */
public abstract class SelfRegisteringListener extends Listener {

	/***/
	static final String identString = "@(#) $Header$";

	/**
	 * @param	classOfEventHandled
	 */
	//public SelfRegisteringListener(Class classOfEventHandled) {
	//	super(classOfEventHandled);
	//	ApplicationEventDispatcher.applicationEventDispatcher.addListener(this);
	//}
	
	/**
	 * @param	classOfEventHandled
	 * @param	eventContext
	 */
	//public SelfRegisteringListener(Class classOfEventHandled,EventContext eventContext) {
	//	super(classOfEventHandled,eventContext);
	//	ApplicationEventDispatcher.applicationEventDispatcher.addListener(this);
	//}

	/**
	 * @param	className
	 * @param	eventContext
	 */
	public SelfRegisteringListener(String className,EventContext eventContext) {
		super(className,eventContext);
		ApplicationEventDispatcher.applicationEventDispatcher.addListener(this);
	}
}

