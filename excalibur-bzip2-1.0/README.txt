                  Avalons Excalibur Bzip2
                  -----------------------

Streams that compress and decompress the BZip2 format (without the
file header chars). Originally derived from code in the ant project.

Getting Started:
----------------

If you downloaded a source release of the component then you
will need to build the component. Directions for building the
component are located in BUILDING.txt

If you downloaded a binary release, or a release with both binary
and source then it is recomended you look over the documentation
in docs/index.html - and then look into the examples/ directory
for examples of the component in action.

